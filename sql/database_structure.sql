-- phpMyAdmin SQL Dump
-- version 3.3.10
-- http://www.phpmyadmin.net
--
-- Serveur: pokedex.sql-pro.online.net
-- Généré le : Ven 07 Décembre 2012 à 23:21
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `pokedex`
--

-- --------------------------------------------------------

--
-- Structure de la table `abilities`
--

DROP TABLE IF EXISTS `abilities`;
CREATE TABLE IF NOT EXISTS `abilities` (
  `id` int(11) NOT NULL,
  `identifier` varchar(24) COLLATE latin1_general_ci NOT NULL,
  `generation_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `generation_id` (`generation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ability_changelog`
--

DROP TABLE IF EXISTS `ability_changelog`;
CREATE TABLE IF NOT EXISTS `ability_changelog` (
  `id` int(11) NOT NULL,
  `ability_id` int(11) NOT NULL,
  `changed_in_version_group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ability_id` (`ability_id`),
  KEY `changed_in_version_group_id` (`changed_in_version_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ability_changelog_prose`
--

DROP TABLE IF EXISTS `ability_changelog_prose`;
CREATE TABLE IF NOT EXISTS `ability_changelog_prose` (
  `ability_changelog_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `effect` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`ability_changelog_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ability_flavor_text`
--

DROP TABLE IF EXISTS `ability_flavor_text`;
CREATE TABLE IF NOT EXISTS `ability_flavor_text` (
  `ability_id` int(11) NOT NULL,
  `version_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `flavor_text` varchar(64) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`ability_id`,`version_group_id`,`language_id`),
  KEY `version_group_id` (`version_group_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ability_names`
--

DROP TABLE IF EXISTS `ability_names`;
CREATE TABLE IF NOT EXISTS `ability_names` (
  `ability_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(24) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`ability_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_ability_names_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ability_prose`
--

DROP TABLE IF EXISTS `ability_prose`;
CREATE TABLE IF NOT EXISTS `ability_prose` (
  `ability_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `effect` text COLLATE latin1_general_ci,
  `short_effect` text COLLATE latin1_general_ci,
  PRIMARY KEY (`ability_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `berries`
--

DROP TABLE IF EXISTS `berries`;
CREATE TABLE IF NOT EXISTS `berries` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `firmness_id` int(11) NOT NULL,
  `natural_gift_power` int(11) DEFAULT NULL,
  `natural_gift_type_id` int(11) DEFAULT NULL,
  `size` int(11) NOT NULL,
  `max_harvest` int(11) NOT NULL,
  `growth_time` int(11) NOT NULL,
  `soil_dryness` int(11) NOT NULL,
  `smoothness` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  KEY `firmness_id` (`firmness_id`),
  KEY `natural_gift_type_id` (`natural_gift_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `berry_firmness`
--

DROP TABLE IF EXISTS `berry_firmness`;
CREATE TABLE IF NOT EXISTS `berry_firmness` (
  `id` int(11) NOT NULL,
  `identifier` varchar(10) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `berry_firmness_names`
--

DROP TABLE IF EXISTS `berry_firmness_names`;
CREATE TABLE IF NOT EXISTS `berry_firmness_names` (
  `berry_firmness_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(10) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`berry_firmness_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_berry_firmness_names_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `berry_flavors`
--

DROP TABLE IF EXISTS `berry_flavors`;
CREATE TABLE IF NOT EXISTS `berry_flavors` (
  `berry_id` int(11) NOT NULL,
  `contest_type_id` int(11) NOT NULL,
  `flavor` int(11) NOT NULL,
  PRIMARY KEY (`berry_id`,`contest_type_id`),
  KEY `contest_type_id` (`contest_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `contest_combos`
--

DROP TABLE IF EXISTS `contest_combos`;
CREATE TABLE IF NOT EXISTS `contest_combos` (
  `first_move_id` int(11) NOT NULL,
  `second_move_id` int(11) NOT NULL,
  PRIMARY KEY (`first_move_id`,`second_move_id`),
  KEY `second_move_id` (`second_move_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `contest_effects`
--

DROP TABLE IF EXISTS `contest_effects`;
CREATE TABLE IF NOT EXISTS `contest_effects` (
  `id` int(11) NOT NULL,
  `appeal` smallint(6) NOT NULL,
  `jam` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `contest_effect_prose`
--

DROP TABLE IF EXISTS `contest_effect_prose`;
CREATE TABLE IF NOT EXISTS `contest_effect_prose` (
  `contest_effect_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `flavor_text` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `effect` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`contest_effect_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `contest_types`
--

DROP TABLE IF EXISTS `contest_types`;
CREATE TABLE IF NOT EXISTS `contest_types` (
  `id` int(11) NOT NULL,
  `identifier` varchar(6) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `contest_type_names`
--

DROP TABLE IF EXISTS `contest_type_names`;
CREATE TABLE IF NOT EXISTS `contest_type_names` (
  `contest_type_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(6) COLLATE latin1_general_ci DEFAULT NULL,
  `flavor` varchar(6) COLLATE latin1_general_ci DEFAULT NULL,
  `color` varchar(6) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`contest_type_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_contest_type_names_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `egg_groups`
--

DROP TABLE IF EXISTS `egg_groups`;
CREATE TABLE IF NOT EXISTS `egg_groups` (
  `id` int(11) NOT NULL,
  `identifier` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `egg_group_prose`
--

DROP TABLE IF EXISTS `egg_group_prose`;
CREATE TABLE IF NOT EXISTS `egg_group_prose` (
  `egg_group_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`egg_group_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_egg_group_prose_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `encounters`
--

DROP TABLE IF EXISTS `encounters`;
CREATE TABLE IF NOT EXISTS `encounters` (
  `id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL,
  `location_area_id` int(11) NOT NULL,
  `encounter_slot_id` int(11) NOT NULL,
  `pokemon_id` int(11) NOT NULL,
  `min_level` int(11) NOT NULL,
  `max_level` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `version_id` (`version_id`),
  KEY `location_area_id` (`location_area_id`),
  KEY `encounter_slot_id` (`encounter_slot_id`),
  KEY `pokemon_id` (`pokemon_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `encounter_conditions`
--

DROP TABLE IF EXISTS `encounter_conditions`;
CREATE TABLE IF NOT EXISTS `encounter_conditions` (
  `id` int(11) NOT NULL,
  `identifier` varchar(64) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `encounter_condition_prose`
--

DROP TABLE IF EXISTS `encounter_condition_prose`;
CREATE TABLE IF NOT EXISTS `encounter_condition_prose` (
  `encounter_condition_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(64) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`encounter_condition_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_encounter_condition_prose_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `encounter_condition_values`
--

DROP TABLE IF EXISTS `encounter_condition_values`;
CREATE TABLE IF NOT EXISTS `encounter_condition_values` (
  `id` int(11) NOT NULL,
  `encounter_condition_id` int(11) NOT NULL,
  `identifier` varchar(64) COLLATE latin1_general_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `encounter_condition_id` (`encounter_condition_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `encounter_condition_value_map`
--

DROP TABLE IF EXISTS `encounter_condition_value_map`;
CREATE TABLE IF NOT EXISTS `encounter_condition_value_map` (
  `encounter_id` int(11) NOT NULL,
  `encounter_condition_value_id` int(11) NOT NULL,
  PRIMARY KEY (`encounter_id`,`encounter_condition_value_id`),
  KEY `encounter_condition_value_id` (`encounter_condition_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `encounter_condition_value_prose`
--

DROP TABLE IF EXISTS `encounter_condition_value_prose`;
CREATE TABLE IF NOT EXISTS `encounter_condition_value_prose` (
  `encounter_condition_value_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(64) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`encounter_condition_value_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_encounter_condition_value_prose_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `encounter_methods`
--

DROP TABLE IF EXISTS `encounter_methods`;
CREATE TABLE IF NOT EXISTS `encounter_methods` (
  `id` int(11) NOT NULL,
  `identifier` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `identifier` (`identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `encounter_method_prose`
--

DROP TABLE IF EXISTS `encounter_method_prose`;
CREATE TABLE IF NOT EXISTS `encounter_method_prose` (
  `encounter_method_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(64) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`encounter_method_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_encounter_method_prose_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `encounter_slots`
--

DROP TABLE IF EXISTS `encounter_slots`;
CREATE TABLE IF NOT EXISTS `encounter_slots` (
  `id` int(11) NOT NULL,
  `version_group_id` int(11) NOT NULL,
  `encounter_method_id` int(11) NOT NULL,
  `slot` int(11) DEFAULT NULL,
  `rarity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `version_group_id` (`version_group_id`),
  KEY `encounter_method_id` (`encounter_method_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `evolution_chains`
--

DROP TABLE IF EXISTS `evolution_chains`;
CREATE TABLE IF NOT EXISTS `evolution_chains` (
  `id` int(11) NOT NULL,
  `baby_trigger_item_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `baby_trigger_item_id` (`baby_trigger_item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `evolution_triggers`
--

DROP TABLE IF EXISTS `evolution_triggers`;
CREATE TABLE IF NOT EXISTS `evolution_triggers` (
  `id` int(11) NOT NULL,
  `identifier` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `evolution_trigger_prose`
--

DROP TABLE IF EXISTS `evolution_trigger_prose`;
CREATE TABLE IF NOT EXISTS `evolution_trigger_prose` (
  `evolution_trigger_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`evolution_trigger_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_evolution_trigger_prose_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `experience`
--

DROP TABLE IF EXISTS `experience`;
CREATE TABLE IF NOT EXISTS `experience` (
  `growth_rate_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `experience` int(11) NOT NULL,
  PRIMARY KEY (`growth_rate_id`,`level`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `generations`
--

DROP TABLE IF EXISTS `generations`;
CREATE TABLE IF NOT EXISTS `generations` (
  `id` int(11) NOT NULL,
  `main_region_id` int(11) NOT NULL,
  `canonical_pokedex_id` int(11) NOT NULL,
  `identifier` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `main_region_id` (`main_region_id`),
  KEY `canonical_pokedex_id` (`canonical_pokedex_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `generation_names`
--

DROP TABLE IF EXISTS `generation_names`;
CREATE TABLE IF NOT EXISTS `generation_names` (
  `generation_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`generation_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_generation_names_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `growth_rates`
--

DROP TABLE IF EXISTS `growth_rates`;
CREATE TABLE IF NOT EXISTS `growth_rates` (
  `id` int(11) NOT NULL,
  `identifier` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `formula` text COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `growth_rate_prose`
--

DROP TABLE IF EXISTS `growth_rate_prose`;
CREATE TABLE IF NOT EXISTS `growth_rate_prose` (
  `growth_rate_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(20) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`growth_rate_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_growth_rate_prose_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `items`
--

DROP TABLE IF EXISTS `items`;
CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL,
  `identifier` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `cost` int(11) NOT NULL,
  `fling_power` int(11) DEFAULT NULL,
  `fling_effect_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `fling_effect_id` (`fling_effect_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `item_categories`
--

DROP TABLE IF EXISTS `item_categories`;
CREATE TABLE IF NOT EXISTS `item_categories` (
  `id` int(11) NOT NULL,
  `pocket_id` int(11) NOT NULL,
  `identifier` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pocket_id` (`pocket_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `item_category_prose`
--

DROP TABLE IF EXISTS `item_category_prose`;
CREATE TABLE IF NOT EXISTS `item_category_prose` (
  `item_category_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`item_category_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_item_category_prose_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `item_flags`
--

DROP TABLE IF EXISTS `item_flags`;
CREATE TABLE IF NOT EXISTS `item_flags` (
  `id` int(11) NOT NULL,
  `identifier` varchar(24) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `item_flag_map`
--

DROP TABLE IF EXISTS `item_flag_map`;
CREATE TABLE IF NOT EXISTS `item_flag_map` (
  `item_id` int(11) NOT NULL,
  `item_flag_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`,`item_flag_id`),
  KEY `item_flag_id` (`item_flag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `item_flag_prose`
--

DROP TABLE IF EXISTS `item_flag_prose`;
CREATE TABLE IF NOT EXISTS `item_flag_prose` (
  `item_flag_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(24) COLLATE latin1_general_ci DEFAULT NULL,
  `description` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`item_flag_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_item_flag_prose_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `item_flavor_summaries`
--

DROP TABLE IF EXISTS `item_flavor_summaries`;
CREATE TABLE IF NOT EXISTS `item_flavor_summaries` (
  `item_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `flavor_summary` text COLLATE latin1_general_ci,
  PRIMARY KEY (`item_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `item_flavor_text`
--

DROP TABLE IF EXISTS `item_flavor_text`;
CREATE TABLE IF NOT EXISTS `item_flavor_text` (
  `item_id` int(11) NOT NULL,
  `version_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `flavor_text` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`item_id`,`version_group_id`,`language_id`),
  KEY `version_group_id` (`version_group_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `item_fling_effects`
--

DROP TABLE IF EXISTS `item_fling_effects`;
CREATE TABLE IF NOT EXISTS `item_fling_effects` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `item_fling_effect_prose`
--

DROP TABLE IF EXISTS `item_fling_effect_prose`;
CREATE TABLE IF NOT EXISTS `item_fling_effect_prose` (
  `item_fling_effect_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `effect` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`item_fling_effect_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `item_game_indices`
--

DROP TABLE IF EXISTS `item_game_indices`;
CREATE TABLE IF NOT EXISTS `item_game_indices` (
  `item_id` int(11) NOT NULL,
  `generation_id` int(11) NOT NULL,
  `game_index` int(11) NOT NULL,
  PRIMARY KEY (`item_id`,`generation_id`),
  KEY `generation_id` (`generation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `item_names`
--

DROP TABLE IF EXISTS `item_names`;
CREATE TABLE IF NOT EXISTS `item_names` (
  `item_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(20) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`item_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_item_names_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `item_pockets`
--

DROP TABLE IF EXISTS `item_pockets`;
CREATE TABLE IF NOT EXISTS `item_pockets` (
  `id` int(11) NOT NULL,
  `identifier` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `item_pocket_names`
--

DROP TABLE IF EXISTS `item_pocket_names`;
CREATE TABLE IF NOT EXISTS `item_pocket_names` (
  `item_pocket_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`item_pocket_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_item_pocket_names_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `item_prose`
--

DROP TABLE IF EXISTS `item_prose`;
CREATE TABLE IF NOT EXISTS `item_prose` (
  `item_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `short_effect` text COLLATE latin1_general_ci,
  `effect` text COLLATE latin1_general_ci,
  PRIMARY KEY (`item_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `languages`
--

DROP TABLE IF EXISTS `languages`;
CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(11) NOT NULL,
  `iso639` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `iso3166` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `identifier` varchar(16) COLLATE latin1_general_ci NOT NULL,
  `official` tinyint(1) NOT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_languages_official` (`official`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `language_names`
--

DROP TABLE IF EXISTS `language_names`;
CREATE TABLE IF NOT EXISTS `language_names` (
  `language_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`language_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_language_names_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `locations`
--

DROP TABLE IF EXISTS `locations`;
CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(11) NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  `identifier` varchar(64) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `region_id` (`region_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `location_areas`
--

DROP TABLE IF EXISTS `location_areas`;
CREATE TABLE IF NOT EXISTS `location_areas` (
  `id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `game_index` int(11) NOT NULL,
  `identifier` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `location_id` (`location_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `location_area_encounter_rates`
--

DROP TABLE IF EXISTS `location_area_encounter_rates`;
CREATE TABLE IF NOT EXISTS `location_area_encounter_rates` (
  `location_area_id` int(11) NOT NULL,
  `encounter_method_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL,
  `rate` int(11) DEFAULT NULL,
  PRIMARY KEY (`location_area_id`,`encounter_method_id`,`version_id`),
  KEY `encounter_method_id` (`encounter_method_id`),
  KEY `version_id` (`version_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `location_area_prose`
--

DROP TABLE IF EXISTS `location_area_prose`;
CREATE TABLE IF NOT EXISTS `location_area_prose` (
  `location_area_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(64) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`location_area_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_location_area_prose_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `location_game_indices`
--

DROP TABLE IF EXISTS `location_game_indices`;
CREATE TABLE IF NOT EXISTS `location_game_indices` (
  `location_id` int(11) NOT NULL,
  `generation_id` int(11) NOT NULL,
  `game_index` int(11) NOT NULL,
  PRIMARY KEY (`location_id`,`generation_id`,`game_index`),
  KEY `generation_id` (`generation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `location_names`
--

DROP TABLE IF EXISTS `location_names`;
CREATE TABLE IF NOT EXISTS `location_names` (
  `location_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(64) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`location_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_location_names_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `machines`
--

DROP TABLE IF EXISTS `machines`;
CREATE TABLE IF NOT EXISTS `machines` (
  `machine_number` int(11) NOT NULL,
  `version_group_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `move_id` int(11) NOT NULL,
  PRIMARY KEY (`machine_number`,`version_group_id`),
  KEY `version_group_id` (`version_group_id`),
  KEY `item_id` (`item_id`),
  KEY `move_id` (`move_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `moves`
--

DROP TABLE IF EXISTS `moves`;
CREATE TABLE IF NOT EXISTS `moves` (
  `id` int(11) NOT NULL,
  `identifier` varchar(24) COLLATE latin1_general_ci NOT NULL,
  `generation_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `power` smallint(6) NOT NULL,
  `pp` smallint(6) DEFAULT NULL,
  `accuracy` smallint(6) DEFAULT NULL,
  `priority` smallint(6) NOT NULL,
  `target_id` int(11) NOT NULL,
  `damage_class_id` int(11) NOT NULL,
  `effect_id` int(11) NOT NULL,
  `effect_chance` int(11) DEFAULT NULL,
  `contest_type_id` int(11) DEFAULT NULL,
  `contest_effect_id` int(11) DEFAULT NULL,
  `super_contest_effect_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `generation_id` (`generation_id`),
  KEY `type_id` (`type_id`),
  KEY `target_id` (`target_id`),
  KEY `damage_class_id` (`damage_class_id`),
  KEY `effect_id` (`effect_id`),
  KEY `contest_type_id` (`contest_type_id`),
  KEY `contest_effect_id` (`contest_effect_id`),
  KEY `super_contest_effect_id` (`super_contest_effect_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_battle_styles`
--

DROP TABLE IF EXISTS `move_battle_styles`;
CREATE TABLE IF NOT EXISTS `move_battle_styles` (
  `id` int(11) NOT NULL,
  `identifier` varchar(8) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_battle_style_prose`
--

DROP TABLE IF EXISTS `move_battle_style_prose`;
CREATE TABLE IF NOT EXISTS `move_battle_style_prose` (
  `move_battle_style_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(8) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`move_battle_style_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_move_battle_style_prose_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_changelog`
--

DROP TABLE IF EXISTS `move_changelog`;
CREATE TABLE IF NOT EXISTS `move_changelog` (
  `move_id` int(11) NOT NULL,
  `changed_in_version_group_id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `power` smallint(6) DEFAULT NULL,
  `pp` smallint(6) DEFAULT NULL,
  `accuracy` smallint(6) DEFAULT NULL,
  `effect_id` int(11) DEFAULT NULL,
  `effect_chance` int(11) DEFAULT NULL,
  PRIMARY KEY (`move_id`,`changed_in_version_group_id`),
  KEY `changed_in_version_group_id` (`changed_in_version_group_id`),
  KEY `type_id` (`type_id`),
  KEY `effect_id` (`effect_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_damage_classes`
--

DROP TABLE IF EXISTS `move_damage_classes`;
CREATE TABLE IF NOT EXISTS `move_damage_classes` (
  `id` int(11) NOT NULL,
  `identifier` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_damage_class_prose`
--

DROP TABLE IF EXISTS `move_damage_class_prose`;
CREATE TABLE IF NOT EXISTS `move_damage_class_prose` (
  `move_damage_class_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `description` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`move_damage_class_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_move_damage_class_prose_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_effects`
--

DROP TABLE IF EXISTS `move_effects`;
CREATE TABLE IF NOT EXISTS `move_effects` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_effect_changelog`
--

DROP TABLE IF EXISTS `move_effect_changelog`;
CREATE TABLE IF NOT EXISTS `move_effect_changelog` (
  `id` int(11) NOT NULL,
  `effect_id` int(11) NOT NULL,
  `changed_in_version_group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `effect_id` (`effect_id`,`changed_in_version_group_id`),
  KEY `changed_in_version_group_id` (`changed_in_version_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_effect_changelog_prose`
--

DROP TABLE IF EXISTS `move_effect_changelog_prose`;
CREATE TABLE IF NOT EXISTS `move_effect_changelog_prose` (
  `move_effect_changelog_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `effect` text COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`move_effect_changelog_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_effect_prose`
--

DROP TABLE IF EXISTS `move_effect_prose`;
CREATE TABLE IF NOT EXISTS `move_effect_prose` (
  `move_effect_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `short_effect` text COLLATE latin1_general_ci,
  `effect` text COLLATE latin1_general_ci,
  PRIMARY KEY (`move_effect_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_flags`
--

DROP TABLE IF EXISTS `move_flags`;
CREATE TABLE IF NOT EXISTS `move_flags` (
  `id` int(11) NOT NULL,
  `identifier` varchar(32) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_flag_map`
--

DROP TABLE IF EXISTS `move_flag_map`;
CREATE TABLE IF NOT EXISTS `move_flag_map` (
  `move_id` int(11) NOT NULL,
  `move_flag_id` int(11) NOT NULL,
  PRIMARY KEY (`move_id`,`move_flag_id`),
  KEY `move_flag_id` (`move_flag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_flag_prose`
--

DROP TABLE IF EXISTS `move_flag_prose`;
CREATE TABLE IF NOT EXISTS `move_flag_prose` (
  `move_flag_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `description` text COLLATE latin1_general_ci,
  PRIMARY KEY (`move_flag_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_move_flag_prose_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_flavor_summaries`
--

DROP TABLE IF EXISTS `move_flavor_summaries`;
CREATE TABLE IF NOT EXISTS `move_flavor_summaries` (
  `move_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `flavor_summary` text COLLATE latin1_general_ci,
  PRIMARY KEY (`move_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_flavor_text`
--

DROP TABLE IF EXISTS `move_flavor_text`;
CREATE TABLE IF NOT EXISTS `move_flavor_text` (
  `move_id` int(11) NOT NULL,
  `version_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `flavor_text` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`move_id`,`version_group_id`,`language_id`),
  KEY `version_group_id` (`version_group_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_meta`
--

DROP TABLE IF EXISTS `move_meta`;
CREATE TABLE IF NOT EXISTS `move_meta` (
  `move_id` int(11) NOT NULL,
  `meta_category_id` int(11) NOT NULL,
  `meta_ailment_id` int(11) NOT NULL,
  `min_hits` int(11) DEFAULT NULL,
  `max_hits` int(11) DEFAULT NULL,
  `min_turns` int(11) DEFAULT NULL,
  `max_turns` int(11) DEFAULT NULL,
  `recoil` int(11) NOT NULL,
  `healing` int(11) NOT NULL,
  `crit_rate` int(11) NOT NULL,
  `ailment_chance` int(11) NOT NULL,
  `flinch_chance` int(11) NOT NULL,
  `stat_chance` int(11) NOT NULL,
  PRIMARY KEY (`move_id`),
  KEY `meta_category_id` (`meta_category_id`),
  KEY `meta_ailment_id` (`meta_ailment_id`),
  KEY `ix_move_meta_ailment_chance` (`ailment_chance`),
  KEY `ix_move_meta_crit_rate` (`crit_rate`),
  KEY `ix_move_meta_flinch_chance` (`flinch_chance`),
  KEY `ix_move_meta_healing` (`healing`),
  KEY `ix_move_meta_max_hits` (`max_hits`),
  KEY `ix_move_meta_max_turns` (`max_turns`),
  KEY `ix_move_meta_min_hits` (`min_hits`),
  KEY `ix_move_meta_min_turns` (`min_turns`),
  KEY `ix_move_meta_recoil` (`recoil`),
  KEY `ix_move_meta_stat_chance` (`stat_chance`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_meta_ailments`
--

DROP TABLE IF EXISTS `move_meta_ailments`;
CREATE TABLE IF NOT EXISTS `move_meta_ailments` (
  `id` int(11) NOT NULL,
  `identifier` varchar(24) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_move_meta_ailments_identifier` (`identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_meta_ailment_names`
--

DROP TABLE IF EXISTS `move_meta_ailment_names`;
CREATE TABLE IF NOT EXISTS `move_meta_ailment_names` (
  `move_meta_ailment_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(24) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`move_meta_ailment_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_move_meta_ailment_names_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_meta_categories`
--

DROP TABLE IF EXISTS `move_meta_categories`;
CREATE TABLE IF NOT EXISTS `move_meta_categories` (
  `id` int(11) NOT NULL,
  `identifier` varchar(32) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_move_meta_categories_identifier` (`identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_meta_category_prose`
--

DROP TABLE IF EXISTS `move_meta_category_prose`;
CREATE TABLE IF NOT EXISTS `move_meta_category_prose` (
  `move_meta_category_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `description` varchar(64) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`move_meta_category_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_meta_stat_changes`
--

DROP TABLE IF EXISTS `move_meta_stat_changes`;
CREATE TABLE IF NOT EXISTS `move_meta_stat_changes` (
  `move_id` int(11) NOT NULL,
  `stat_id` int(11) NOT NULL,
  `change` int(11) NOT NULL,
  PRIMARY KEY (`move_id`,`stat_id`),
  KEY `stat_id` (`stat_id`),
  KEY `ix_move_meta_stat_changes_change` (`change`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_names`
--

DROP TABLE IF EXISTS `move_names`;
CREATE TABLE IF NOT EXISTS `move_names` (
  `move_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(24) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`move_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_move_names_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_targets`
--

DROP TABLE IF EXISTS `move_targets`;
CREATE TABLE IF NOT EXISTS `move_targets` (
  `id` int(11) NOT NULL,
  `identifier` varchar(32) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `move_target_prose`
--

DROP TABLE IF EXISTS `move_target_prose`;
CREATE TABLE IF NOT EXISTS `move_target_prose` (
  `move_target_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`move_target_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_move_target_prose_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `natures`
--

DROP TABLE IF EXISTS `natures`;
CREATE TABLE IF NOT EXISTS `natures` (
  `id` int(11) NOT NULL,
  `identifier` varchar(8) COLLATE latin1_general_ci NOT NULL,
  `decreased_stat_id` int(11) NOT NULL,
  `increased_stat_id` int(11) NOT NULL,
  `hates_flavor_id` int(11) NOT NULL,
  `likes_flavor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `decreased_stat_id` (`decreased_stat_id`),
  KEY `increased_stat_id` (`increased_stat_id`),
  KEY `hates_flavor_id` (`hates_flavor_id`),
  KEY `likes_flavor_id` (`likes_flavor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `nature_battle_style_preferences`
--

DROP TABLE IF EXISTS `nature_battle_style_preferences`;
CREATE TABLE IF NOT EXISTS `nature_battle_style_preferences` (
  `nature_id` int(11) NOT NULL,
  `move_battle_style_id` int(11) NOT NULL,
  `low_hp_preference` int(11) NOT NULL,
  `high_hp_preference` int(11) NOT NULL,
  PRIMARY KEY (`nature_id`,`move_battle_style_id`),
  KEY `move_battle_style_id` (`move_battle_style_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `nature_names`
--

DROP TABLE IF EXISTS `nature_names`;
CREATE TABLE IF NOT EXISTS `nature_names` (
  `nature_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(8) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`nature_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_nature_names_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `nature_pokeathlon_stats`
--

DROP TABLE IF EXISTS `nature_pokeathlon_stats`;
CREATE TABLE IF NOT EXISTS `nature_pokeathlon_stats` (
  `nature_id` int(11) NOT NULL,
  `pokeathlon_stat_id` int(11) NOT NULL,
  `max_change` int(11) NOT NULL,
  PRIMARY KEY (`nature_id`,`pokeathlon_stat_id`),
  KEY `pokeathlon_stat_id` (`pokeathlon_stat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokeathlon_stats`
--

DROP TABLE IF EXISTS `pokeathlon_stats`;
CREATE TABLE IF NOT EXISTS `pokeathlon_stats` (
  `id` int(11) NOT NULL,
  `identifier` varchar(8) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokeathlon_stat_names`
--

DROP TABLE IF EXISTS `pokeathlon_stat_names`;
CREATE TABLE IF NOT EXISTS `pokeathlon_stat_names` (
  `pokeathlon_stat_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(8) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`pokeathlon_stat_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_pokeathlon_stat_names_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokedexes`
--

DROP TABLE IF EXISTS `pokedexes`;
CREATE TABLE IF NOT EXISTS `pokedexes` (
  `id` int(11) NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  `identifier` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `region_id` (`region_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokedex_prose`
--

DROP TABLE IF EXISTS `pokedex_prose`;
CREATE TABLE IF NOT EXISTS `pokedex_prose` (
  `pokedex_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `description` text COLLATE latin1_general_ci,
  PRIMARY KEY (`pokedex_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_pokedex_prose_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon`
--

DROP TABLE IF EXISTS `pokemon`;
CREATE TABLE IF NOT EXISTS `pokemon` (
  `id` int(11) NOT NULL,
  `species_id` int(11) DEFAULT NULL,
  `height` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  `base_experience` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `is_default` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `species_id` (`species_id`),
  KEY `ix_pokemon_is_default` (`is_default`),
  KEY `ix_pokemon_order` (`order`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_abilities`
--

DROP TABLE IF EXISTS `pokemon_abilities`;
CREATE TABLE IF NOT EXISTS `pokemon_abilities` (
  `pokemon_id` int(11) NOT NULL,
  `ability_id` int(11) NOT NULL,
  `is_dream` tinyint(1) NOT NULL,
  `slot` int(11) NOT NULL,
  PRIMARY KEY (`pokemon_id`,`slot`),
  KEY `ability_id` (`ability_id`),
  KEY `ix_pokemon_abilities_is_dream` (`is_dream`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_colors`
--

DROP TABLE IF EXISTS `pokemon_colors`;
CREATE TABLE IF NOT EXISTS `pokemon_colors` (
  `id` int(11) NOT NULL,
  `identifier` varchar(6) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_color_names`
--

DROP TABLE IF EXISTS `pokemon_color_names`;
CREATE TABLE IF NOT EXISTS `pokemon_color_names` (
  `pokemon_color_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(6) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`pokemon_color_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_pokemon_color_names_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_dex_numbers`
--

DROP TABLE IF EXISTS `pokemon_dex_numbers`;
CREATE TABLE IF NOT EXISTS `pokemon_dex_numbers` (
  `species_id` int(11) NOT NULL,
  `pokedex_id` int(11) NOT NULL,
  `pokedex_number` int(11) NOT NULL,
  PRIMARY KEY (`species_id`,`pokedex_id`),
  KEY `pokedex_id` (`pokedex_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_egg_groups`
--

DROP TABLE IF EXISTS `pokemon_egg_groups`;
CREATE TABLE IF NOT EXISTS `pokemon_egg_groups` (
  `species_id` int(11) NOT NULL,
  `egg_group_id` int(11) NOT NULL,
  PRIMARY KEY (`species_id`,`egg_group_id`),
  KEY `egg_group_id` (`egg_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_evolution`
--

DROP TABLE IF EXISTS `pokemon_evolution`;
CREATE TABLE IF NOT EXISTS `pokemon_evolution` (
  `id` int(11) NOT NULL,
  `evolved_species_id` int(11) NOT NULL,
  `evolution_trigger_id` int(11) NOT NULL,
  `trigger_item_id` int(11) DEFAULT NULL,
  `minimum_level` int(11) DEFAULT NULL,
  `gender` varchar(6) COLLATE latin1_general_ci DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `held_item_id` int(11) DEFAULT NULL,
  `time_of_day` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `known_move_id` int(11) DEFAULT NULL,
  `minimum_happiness` int(11) DEFAULT NULL,
  `minimum_beauty` int(11) DEFAULT NULL,
  `relative_physical_stats` int(11) DEFAULT NULL,
  `party_species_id` int(11) DEFAULT NULL,
  `trade_species_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `evolved_species_id` (`evolved_species_id`),
  KEY `evolution_trigger_id` (`evolution_trigger_id`),
  KEY `trigger_item_id` (`trigger_item_id`),
  KEY `location_id` (`location_id`),
  KEY `held_item_id` (`held_item_id`),
  KEY `known_move_id` (`known_move_id`),
  KEY `party_species_id` (`party_species_id`),
  KEY `trade_species_id` (`trade_species_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_forms`
--

DROP TABLE IF EXISTS `pokemon_forms`;
CREATE TABLE IF NOT EXISTS `pokemon_forms` (
  `id` int(11) NOT NULL,
  `form_identifier` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `pokemon_id` int(11) NOT NULL,
  `introduced_in_version_group_id` int(11) DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL,
  `is_battle_only` tinyint(1) NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pokemon_id` (`pokemon_id`),
  KEY `introduced_in_version_group_id` (`introduced_in_version_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_form_generations`
--

DROP TABLE IF EXISTS `pokemon_form_generations`;
CREATE TABLE IF NOT EXISTS `pokemon_form_generations` (
  `pokemon_form_id` int(11) NOT NULL,
  `generation_id` int(11) NOT NULL,
  `game_index` int(11) NOT NULL,
  PRIMARY KEY (`pokemon_form_id`,`generation_id`),
  KEY `generation_id` (`generation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_form_names`
--

DROP TABLE IF EXISTS `pokemon_form_names`;
CREATE TABLE IF NOT EXISTS `pokemon_form_names` (
  `pokemon_form_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `form_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `pokemon_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`pokemon_form_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_pokemon_form_names_form_name` (`form_name`),
  KEY `ix_pokemon_form_names_pokemon_name` (`pokemon_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_form_pokeathlon_stats`
--

DROP TABLE IF EXISTS `pokemon_form_pokeathlon_stats`;
CREATE TABLE IF NOT EXISTS `pokemon_form_pokeathlon_stats` (
  `pokemon_form_id` int(11) NOT NULL,
  `pokeathlon_stat_id` int(11) NOT NULL,
  `minimum_stat` int(11) NOT NULL,
  `base_stat` int(11) NOT NULL,
  `maximum_stat` int(11) NOT NULL,
  PRIMARY KEY (`pokemon_form_id`,`pokeathlon_stat_id`),
  KEY `pokeathlon_stat_id` (`pokeathlon_stat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_game_indices`
--

DROP TABLE IF EXISTS `pokemon_game_indices`;
CREATE TABLE IF NOT EXISTS `pokemon_game_indices` (
  `pokemon_id` int(11) NOT NULL,
  `generation_id` int(11) NOT NULL,
  `game_index` int(11) NOT NULL,
  PRIMARY KEY (`pokemon_id`,`generation_id`),
  KEY `generation_id` (`generation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_habitats`
--

DROP TABLE IF EXISTS `pokemon_habitats`;
CREATE TABLE IF NOT EXISTS `pokemon_habitats` (
  `id` int(11) NOT NULL,
  `identifier` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_habitat_names`
--

DROP TABLE IF EXISTS `pokemon_habitat_names`;
CREATE TABLE IF NOT EXISTS `pokemon_habitat_names` (
  `pokemon_habitat_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`pokemon_habitat_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_pokemon_habitat_names_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_items`
--

DROP TABLE IF EXISTS `pokemon_items`;
CREATE TABLE IF NOT EXISTS `pokemon_items` (
  `pokemon_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `rarity` int(11) NOT NULL,
  PRIMARY KEY (`pokemon_id`,`version_id`,`item_id`),
  KEY `version_id` (`version_id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_moves`
--

DROP TABLE IF EXISTS `pokemon_moves`;
CREATE TABLE IF NOT EXISTS `pokemon_moves` (
  `pokemon_id` int(11) NOT NULL,
  `version_group_id` int(11) NOT NULL,
  `move_id` int(11) NOT NULL,
  `pokemon_move_method_id` int(11) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`pokemon_id`,`version_group_id`,`move_id`,`pokemon_move_method_id`,`level`),
  KEY `ix_pokemon_moves_level` (`level`),
  KEY `ix_pokemon_moves_move_id` (`move_id`),
  KEY `ix_pokemon_moves_pokemon_id` (`pokemon_id`),
  KEY `ix_pokemon_moves_pokemon_move_method_id` (`pokemon_move_method_id`),
  KEY `ix_pokemon_moves_version_group_id` (`version_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_move_methods`
--

DROP TABLE IF EXISTS `pokemon_move_methods`;
CREATE TABLE IF NOT EXISTS `pokemon_move_methods` (
  `id` int(11) NOT NULL,
  `identifier` varchar(64) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_move_method_prose`
--

DROP TABLE IF EXISTS `pokemon_move_method_prose`;
CREATE TABLE IF NOT EXISTS `pokemon_move_method_prose` (
  `pokemon_move_method_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `description` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`pokemon_move_method_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_pokemon_move_method_prose_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_shapes`
--

DROP TABLE IF EXISTS `pokemon_shapes`;
CREATE TABLE IF NOT EXISTS `pokemon_shapes` (
  `id` int(11) NOT NULL,
  `identifier` varchar(24) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_shape_prose`
--

DROP TABLE IF EXISTS `pokemon_shape_prose`;
CREATE TABLE IF NOT EXISTS `pokemon_shape_prose` (
  `pokemon_shape_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(24) COLLATE latin1_general_ci DEFAULT NULL,
  `awesome_name` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`pokemon_shape_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_pokemon_shape_prose_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_species`
--

DROP TABLE IF EXISTS `pokemon_species`;
CREATE TABLE IF NOT EXISTS `pokemon_species` (
  `id` int(11) NOT NULL,
  `identifier` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `generation_id` int(11) DEFAULT NULL,
  `evolves_from_species_id` int(11) DEFAULT NULL,
  `evolution_chain_id` int(11) DEFAULT NULL,
  `color_id` int(11) NOT NULL,
  `shape_id` int(11) NOT NULL,
  `habitat_id` int(11) DEFAULT NULL,
  `gender_rate` int(11) NOT NULL,
  `capture_rate` int(11) NOT NULL,
  `base_happiness` int(11) NOT NULL,
  `is_baby` tinyint(1) NOT NULL,
  `hatch_counter` int(11) NOT NULL,
  `has_gender_differences` tinyint(1) NOT NULL,
  `growth_rate_id` int(11) NOT NULL,
  `forms_switchable` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `generation_id` (`generation_id`),
  KEY `evolves_from_species_id` (`evolves_from_species_id`),
  KEY `evolution_chain_id` (`evolution_chain_id`),
  KEY `color_id` (`color_id`),
  KEY `shape_id` (`shape_id`),
  KEY `habitat_id` (`habitat_id`),
  KEY `growth_rate_id` (`growth_rate_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_species_flavor_text`
--

DROP TABLE IF EXISTS `pokemon_species_flavor_text`;
CREATE TABLE IF NOT EXISTS `pokemon_species_flavor_text` (
  `species_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `flavor_text` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`species_id`,`version_id`,`language_id`),
  KEY `version_id` (`version_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_species_names`
--

DROP TABLE IF EXISTS `pokemon_species_names`;
CREATE TABLE IF NOT EXISTS `pokemon_species_names` (
  `pokemon_species_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `genus` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`pokemon_species_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_species_prose`
--

DROP TABLE IF EXISTS `pokemon_species_prose`;
CREATE TABLE IF NOT EXISTS `pokemon_species_prose` (
  `pokemon_species_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `form_description` text COLLATE latin1_general_ci,
  PRIMARY KEY (`pokemon_species_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_stats`
--

DROP TABLE IF EXISTS `pokemon_stats`;
CREATE TABLE IF NOT EXISTS `pokemon_stats` (
  `pokemon_id` int(11) NOT NULL,
  `stat_id` int(11) NOT NULL,
  `base_stat` int(11) NOT NULL,
  `effort` int(11) NOT NULL,
  PRIMARY KEY (`pokemon_id`,`stat_id`),
  KEY `stat_id` (`stat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_types`
--

DROP TABLE IF EXISTS `pokemon_types`;
CREATE TABLE IF NOT EXISTS `pokemon_types` (
  `pokemon_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `slot` int(11) NOT NULL,
  PRIMARY KEY (`pokemon_id`,`slot`),
  KEY `type_id` (`type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `regions`
--

DROP TABLE IF EXISTS `regions`;
CREATE TABLE IF NOT EXISTS `regions` (
  `id` int(11) NOT NULL,
  `identifier` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `region_names`
--

DROP TABLE IF EXISTS `region_names`;
CREATE TABLE IF NOT EXISTS `region_names` (
  `region_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`region_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_region_names_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `stats`
--

DROP TABLE IF EXISTS `stats`;
CREATE TABLE IF NOT EXISTS `stats` (
  `id` int(11) NOT NULL,
  `damage_class_id` int(11) DEFAULT NULL,
  `identifier` varchar(16) COLLATE latin1_general_ci NOT NULL,
  `is_battle_only` tinyint(1) NOT NULL,
  `game_index` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `damage_class_id` (`damage_class_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `stat_hints`
--

DROP TABLE IF EXISTS `stat_hints`;
CREATE TABLE IF NOT EXISTS `stat_hints` (
  `id` int(11) NOT NULL,
  `stat_id` int(11) NOT NULL,
  `gene_mod_5` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stat_id` (`stat_id`),
  KEY `ix_stat_hints_gene_mod_5` (`gene_mod_5`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `stat_hint_names`
--

DROP TABLE IF EXISTS `stat_hint_names`;
CREATE TABLE IF NOT EXISTS `stat_hint_names` (
  `stat_hint_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `message` varchar(24) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`stat_hint_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_stat_hint_names_message` (`message`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `stat_names`
--

DROP TABLE IF EXISTS `stat_names`;
CREATE TABLE IF NOT EXISTS `stat_names` (
  `stat_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`stat_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_stat_names_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `super_contest_combos`
--

DROP TABLE IF EXISTS `super_contest_combos`;
CREATE TABLE IF NOT EXISTS `super_contest_combos` (
  `first_move_id` int(11) NOT NULL,
  `second_move_id` int(11) NOT NULL,
  PRIMARY KEY (`first_move_id`,`second_move_id`),
  KEY `second_move_id` (`second_move_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `super_contest_effects`
--

DROP TABLE IF EXISTS `super_contest_effects`;
CREATE TABLE IF NOT EXISTS `super_contest_effects` (
  `id` int(11) NOT NULL,
  `appeal` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `super_contest_effect_prose`
--

DROP TABLE IF EXISTS `super_contest_effect_prose`;
CREATE TABLE IF NOT EXISTS `super_contest_effect_prose` (
  `super_contest_effect_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `flavor_text` varchar(64) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`super_contest_effect_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `types`
--

DROP TABLE IF EXISTS `types`;
CREATE TABLE IF NOT EXISTS `types` (
  `id` int(11) NOT NULL,
  `identifier` varchar(12) COLLATE latin1_general_ci NOT NULL,
  `generation_id` int(11) NOT NULL,
  `damage_class_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `generation_id` (`generation_id`),
  KEY `damage_class_id` (`damage_class_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `type_efficacy`
--

DROP TABLE IF EXISTS `type_efficacy`;
CREATE TABLE IF NOT EXISTS `type_efficacy` (
  `damage_type_id` int(11) NOT NULL,
  `target_type_id` int(11) NOT NULL,
  `damage_factor` int(11) NOT NULL,
  PRIMARY KEY (`damage_type_id`,`target_type_id`),
  KEY `target_type_id` (`target_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `type_names`
--

DROP TABLE IF EXISTS `type_names`;
CREATE TABLE IF NOT EXISTS `type_names` (
  `type_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(12) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`type_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_type_names_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `versions`
--

DROP TABLE IF EXISTS `versions`;
CREATE TABLE IF NOT EXISTS `versions` (
  `id` int(11) NOT NULL,
  `version_group_id` int(11) NOT NULL,
  `identifier` varchar(32) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `version_group_id` (`version_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `version_groups`
--

DROP TABLE IF EXISTS `version_groups`;
CREATE TABLE IF NOT EXISTS `version_groups` (
  `id` int(11) NOT NULL,
  `generation_id` int(11) NOT NULL,
  `pokedex_id` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `generation_id` (`generation_id`),
  KEY `pokedex_id` (`pokedex_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `version_group_pokemon_move_methods`
--

DROP TABLE IF EXISTS `version_group_pokemon_move_methods`;
CREATE TABLE IF NOT EXISTS `version_group_pokemon_move_methods` (
  `version_group_id` int(11) NOT NULL,
  `pokemon_move_method_id` int(11) NOT NULL,
  PRIMARY KEY (`version_group_id`,`pokemon_move_method_id`),
  KEY `pokemon_move_method_id` (`pokemon_move_method_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `version_group_regions`
--

DROP TABLE IF EXISTS `version_group_regions`;
CREATE TABLE IF NOT EXISTS `version_group_regions` (
  `version_group_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  PRIMARY KEY (`version_group_id`,`region_id`),
  KEY `region_id` (`region_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `version_names`
--

DROP TABLE IF EXISTS `version_names`;
CREATE TABLE IF NOT EXISTS `version_names` (
  `version_id` int(11) NOT NULL,
  `local_language_id` int(11) NOT NULL,
  `name` varchar(32) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`version_id`,`local_language_id`),
  KEY `local_language_id` (`local_language_id`),
  KEY `ix_version_names_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
