<?php 

	// Désactiver le rapport d'erreurs
	error_reporting(0);
	
	if (($_SERVER['SERVER_NAME'] == '127.0.0.1') OR ($_SERVER['SERVER_NAME'] == 'localhost')) {
		$PARAM_hote='localhost'; // le chemin vers le serveur
		$PARAM_port='3306';
		$PARAM_nom_bd='pokedex'; // le nom de votre base de données
		$PARAM_utilisateur='root'; // nom d'utilisateur pour se connecter
		$PARAM_mot_passe='access'; // mot de passe de l'utilisateur pour se connecter
	} else {
		$PARAM_hote='pokedex.julientissier.fr'; // le chemin vers le serveur
		$PARAM_port='3306';
		$PARAM_nom_bd='pokedex'; // le nom de votre base de données
		$PARAM_utilisateur='pokedex'; // nom d'utilisateur pour se connecter
		$PARAM_mot_passe='P@ssw0rd'; // mot de passe de l'utilisateur pour se connecter
	}
		
	$PKMN_num = $_GET['n'];
	$lng = 5; //5: français
	$pokemon = Array();
	
	//try {
		$connexion = new PDO('mysql:host='.$PARAM_hote.';dbname='.$PARAM_nom_bd, $PARAM_utilisateur, $PARAM_mot_passe);
		$connexion->query("SET NAMES utf8");
		//name
		$sql  = "SELECT name "; 
		$sql .= "FROM `pokemon_species_names` PSN ";
		$sql .= "WHERE PSN.pokemon_species_id = $PKMN_num ";  
		$sql .= "AND PSN.local_language_id = $lng ";
		
		//echo $sql;
		
		$statement=$connexion->prepare($sql);
		$statement->execute();
		
		$pokemon = $statement->fetchAll(PDO::FETCH_ASSOC);
		
		$statement->closeCursor(); 
		
		$json=json_encode($pokemon[0]);
		
		echo $json;
		
	   
	   
	/*} catch(Exception $e) {
		echo 'Erreur : '.$e->getMessage().'<br />';
	}*/

?>