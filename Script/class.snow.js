
function Snow() { //main canvas
	this.canvas = null;
    this.context = null;
    this.bufferCanvas = null;
    this.bufferCanvasCtx = null;
    this.flakeArray = [];
    this.flakeTimer = null;
    this.maxFlakes = 200; // Here you may set max flackes to be created 

    function init() {
		this.canvas = document.createElement('canvas'); //
        this.context = canvas.getContext("2d");

        this.bufferCanvas = document.createElement("canvas");
        this.bufferCanvasCtx = bufferCanvas.getContext("2d");
        this.bufferCanvasCtx.canvas.width = context.canvas.width;
        this.bufferCanvasCtx.canvas.height = context.canvas.height;

        var snow = this;
        this.flakeTimer = setInterval(snow.addFlake(), 200);

        this.Draw();

        setInterval(animate, 30);
         
    }
    function animate() {
        
        Update();
        Draw();
        
    }
    function addFlake() {

        flakeArray[flakeArray.length] = new Flake();
        if (flakeArray.length == maxFlakes)
            clearInterval(flakeTimer);
    }
    function blank() {
        bufferCanvasCtx.fillStyle = "rgba(0,0,0,0.8)";
        bufferCanvasCtx.fillRect(0, 0, bufferCanvasCtx.canvas.width, bufferCanvasCtx.canvas.height);
        
    }
    function Update() {
        for (var i = 0; i < flakeArray.length; i++) {
            if (flakeArray[i].y < context.canvas.height) {
                flakeArray[i].y += flakeArray[i].speed;
                if (flakeArray[i].y > context.canvas.height)
                    flakeArray[i].y = -5;
                flakeArray[i].x += flakeArray[i].drift;
                if (flakeArray[i].x > context.canvas.width)
                    flakeArray[i].x = 0;
            }
        }
        
    }
    function Flake() {
        this.x = Math.round(Math.random() * context.canvas.width);
        this.y = -10;
        this.drift = Math.random();
        this.speed = Math.round(Math.random() * 5) + 1;
        this.width = (Math.random() * 3) + 2;
        this.height = this.width;
    }
    function Draw() {
        context.save();
        
        blank();

        for (var i = 0; i < flakeArray.length; i++) {
            bufferCanvasCtx.fillStyle = "white";
            bufferCanvasCtx.fillRect(flakeArray[i].x, flakeArray[i].y, flakeArray[i].width, flakeArray[i].height);
        }

        
        context.drawImage(bufferCanvas, 0, 0, bufferCanvas.width, bufferCanvas.height);
        context.restore();
    }
}