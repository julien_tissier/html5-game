//jQuery.noConflict();

var main;
var mainIndex = 4;

function basename(path) {
    return path.replace(/\\/g,'/').replace( /.*\//, '' );
}

function dirname(path) {
    return path.replace(/\\/g,'/').replace(/\/[^\/]*$/, '');;
}

function textWidth(text, fontStyle) {
	//var oPlaceName = document.getElementById("place-name");
	var iWidth = 0;
	
	var oPlaceName = document.createElement("p");
	
	oPlaceName.appendChild(document.createTextNode(text));	
	
	oPlaceName.setAttribute("id", "place-name");
	oPlaceName.style.position = "absolute";
	oPlaceName.style.height = "auto";
	oPlaceName.style.width = "auto";
	
	if(fontStyle==undefined){
		fontStyle='10px pokemon_rs_intlregular';
	}
	oPlaceName.style.font = fontStyle;
	
	document.body.appendChild(oPlaceName);	
	
	if (oPlaceName){
		iWidth = oPlaceName.clientWidth + 1;
		removeDOMElement("place-name");
	}
	
	return iWidth;
}

function textHeight(text, fontStyle) {
	//var oPlaceName = document.getElementById("place-name");
	var iHeight = 0;
	
	var oPlaceName = document.createElement("p");
	
	oPlaceName.appendChild(document.createTextNode(text));	
	
	oPlaceName.setAttribute("id", "place-name");
	oPlaceName.style.position = "absolute";
	oPlaceName.style.height = "auto";
	oPlaceName.style.width = "auto";
	
	if(fontStyle==undefined){
		fontStyle='10px pokemon_rs_intlregular';
	}
	oPlaceName.style.font = fontStyle;
	
	document.body.appendChild(oPlaceName);	
	
	if (oPlaceName){
		iHeight = oPlaceName.clientHeight + 1;
		removeDOMElement("place-name");
	}
	
	return iHeight;
}


function removeDOMElement(EId) {
    return(EObj=document.getElementById(EId))?EObj.parentNode.removeChild(EObj):false;
}

function getXMLHttpRequest() {
	var xhr = null;
	
	if (window.XMLHttpRequest || window.ActiveXObject) {
		if (window.ActiveXObject) {
			try {
				xhr = new ActiveXObject("Msxml2.XMLHTTP");
			} catch(e) {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
		} else {
			xhr = new XMLHttpRequest(); 
		}
	} else {
		alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
		return null;
	}
	
	return xhr;
}

jQuery(document).ready(function($) {


	main = document.getElementById('main');

	map = 'Graphics/maps/kanto.png';
	tiledMap=  'Graphics/maps/tMap.json';
	
	rpg = new Game();
	rpg.init('main');
	rpg.loadMap(map);
	rpg.loadTiledMap(tiledMap);	
	
	rpg.mute = true;
	rpg.audio.muted = rpg.mute;
		
	/*************************** Tiles ***************************/
	//Game.addTile(TileX, TileY, TileWidth, TileHeight, GameX, GameY, source);

	//rpg.addTile(0, 4280, 164, 38, 0, 118, TileSet);	// centre de soins
	//rpg.addForeground(0, 4162, 164, 118, 0, 0, TileSet); //toit centre de soins
	//rpg.addObstacle(0, 32, 164, 112, 1); //obstacle centre de soins
	//rpg.addObstacle(64, 120, 32, 32, "door"); //porte centre de soins

	rpg.addTile(0, 3632, 94, 108, 128, 32, TileSet); // arbre
	rpg.addForeground(0, 3632, 94, 88, 128, 32, TileSet); //feuillage arbre
	rpg.addObstacle(146, 92, 57, 48, 1); //obstacle arbre	

	rpg.addTile(0, 3632, 94, 108, 1028, 32, TileSet); // arbre
	rpg.addForeground(0, 3632, 94, 88, 1028, 32, TileSet); //feuillage arbre
	rpg.addObstacle(1046, 92, 57, 48, 1); //obstacle arbre	

	/*rpg.addTile(0, 3632, 256, 108, 256, 32, TileSet); // arbre
	rpg.addForeground(0, 3632, 256, 88, 256, 32, TileSet); //feuillage arbre
	rpg.addObstacle(436, 92, 57, 48, 1); //obstacle arbre		

	rpg.addTile(0, 4800, 208, 192, 600, 0, TileSet); // maison avec escalier	
	rpg.addForeground(0, 4800, 160, 124, 600, 0, TileSet); // toit maison avec escalier

	rpg.addObstacle(600, 56, 160, 128, 1); //obstacle maison avec escalier
	rpg.addObstacle(730, 56, 80, 40, 1); //obstacle haut escalier
	rpg.addObstacle(784, 56, 50, 128, 1); //obstacle rambarde escalier
	rpg.addObstacle(632, 160, 32, 32, "door"); //porte maison avec escalier

	rpg.addTile(0, 160, 96, 96, 256, 256, TileSet); // hautes herbes
	rpg.addObstacle(256, 256, 96, 96, "grass"); // hautes herbes

	rpg.addTile(160, 8824, 64, 40, 680, 190, TileSet);// banc*/

	
	rpg.addObstacle(0, 0, 128, 1280, 1);
	rpg.addObstacle(128, 296, 64, 984, 1);
	rpg.addObstacle(0, 0, 1536, 48, 1);
	rpg.addObstacle(128, 208, 96, 32, 1);
	rpg.addObstacle(280, 208, 240, 32, 1);
	rpg.addObstacle(384, 8, 352, 200, 1);
	rpg.addObstacle(608, 208, 576, 32, 1);

	rpg.addObstacle(288, 392, 64, 128, 1);
	rpg.addObstacle(384, 376, 224, 128, 1);
	rpg.addObstacle(640, 392, 64, 128, 1);

	rpg.addObstacle(192, 612, 516, 32, 1);
	rpg.addObstacle(1280, 0, 256, 628, 1);
	rpg.addObstacle(1024, 260, 160, 76, 1);
	rpg.addObstacle(832, 472, 128, 88, 1);
	
	
	rpg.addObstacle(1280, 628, 256, 128, "changemap");
	rpg.addObstacle(0, 4340, 168, 128, "changemap");
	
	
	rpg.addObstacle(1280, 756, 256, 520, 1);
	rpg.addObstacle(0, 1136, 640, 192, 1);
	
	rpg.addObstacle(770, 1136, 640, 192, 1);
	
	
	rpg.addObstacle(448, 1332, 224, 224, "grass");
	
	
	rpg.addForeground(1024, 260, 160, 76, 1024, 260, TileSetBasic); // arbre
	
	rpg.addForeground(192, 0, 64, 64, 128, 276, TileSetBasic); // arbre
	rpg.addForeground(384, 374, 224, 94, 384, 374, map); //feuillage arbre	
	//rpg.addForeground(132, 280, 57, 40, 132, 280, map); //feuillage arbre	
	/*************************** Sprites ***************************/

	mainSprite = rpg.addSprite(256, 256, characters[mainIndex], true);

	rpg.launch();
	
	$("#sound").click(function() {
		rpg.mute = !rpg.mute;
		rpg.audio.muted = rpg.mute;
	});
	
	/*if (window.WebSocket) {
		socket= new WebSocket('ws://www.example.com:8000/somesocket'); 
		
		socket.onopen= function() { 
			socket.send('hello'); 
		}; 
		socket.onmessage= function(s) { 
			alert('got reply '+s); 
		}; 
	} else {
	  alert("Votre navigateur ne supporte pas le protocole WebSocket.");
	}*/

	//
});