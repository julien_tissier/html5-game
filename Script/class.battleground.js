/*
Classe Battleground
Permet de cr�er les combattants
*/
 
function Battleground() {  
	
	this.id; 				// id
	
	this.parent;			//main canvas
	this.canvas;			//canvas to hold this Battleground - will be drawn to main canvas
	
	this.imagePath = "Graphics/Battlegrounds/Tiles/";
	
	this.ctx;				//context for Battleground canvas
	
	this.x = 0;
	this.y = 0;
	
	this.destinationX;		
	this.destinationY;		
	
	this.width = 0; 		
	this.height = 0;		
	
	this.image;				//image that is being drawn to the canvas
	this.is_ready = 0;		//Battleground has finished loading and can be used
	this.orientation;
	
	this.init = function(parent, number, direction){ //initialize Battleground
		
		this.number = number.toString(); //cast number in string
		
		this.is_ready = false; //Battleground not ready
		
		this.orientation = direction;
		
		this.parent = parent; //parent is the main canvas wich invoque
		this.canvas = document.createElement('canvas'); //
		this.ctx = this.canvas.getContext('2d'); //get canvas drawing context
		
		this.loadImage(this.imagePath+"battle"+this.number+".png"); // load image for Battleground
		
		if ( typeof Battleground.id == 'undefined' ) Battleground.id = 0;
		
		//appeler le nom de la classe comme variable statique
		this.id = Battleground.id++;
		
		console.log("loading "+this.orientation+" Battleground "+this.id);
	}
	
	this.loadImage = function(img_file){  //loads image to draw for Battleground
		
		this.image = new Image();  //create new image object
		this.image.src = img_file; //load file into image object
		
		
		var battleground = this;
		
		this.image.onload = function(){  //event handler for image load 
			battleground.width = this.width;
			battleground.height = this.height;
			
			if(battleground.orientation === "Back") {
				battleground.destinationX = 0;
				battleground.destinationY = battleground.parent.height/1.3-this.height;
			} else {
				battleground.destinationX = battleground.parent.width-this.width+80;
				battleground.destinationY = 80;
			}
			console.log(battleground.image.width, battleground.image.height);
			console.log(battleground.destinationX, battleground.destinationY);
		
		
			battleground.is_ready = true; // Battleground is ready when image is loaded
		}
		
		return battleground;
	}
	
	this.drawImage = function(){ //draw image into Battleground canvas
		
		this.ctx.clearRect(0,0,this.image.width,this.image.height); //clear previous frame
		
		if(this.is_ready){ //do not draw if Battleground is not ready
			this.ctx.drawImage(this.image, 0, 0, this.image.width-80, this.image.height);
		} 
	}

}