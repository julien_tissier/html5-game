function Tile() {  
	
	this.id; 				// id
	this.parent;			//main canvas
	this.canvas;			//canvas to hold this tile - will be drawn to main canvas
	this.ctx;				//context for tile canvas
	this.x = 0;				// X position of this tile
	this.y = 0;				//Y position of this tile
	this.destinationX;		
	this.destinationY;		
	this.width = 0; 		
	this.height = 0;		
	this.image;				//image that is being drawn to the canvas
	this.is_ready = 0;		//tile has finished loading and can be used
	
	this.init = function(parent, x, y, width, height, destinationX, destinationY, img_file){ //initialize tile
		
		this.is_ready = false; //tile not ready
		this.x = x;
		this.y = y;
		
		this.width = width;
		this.height = height;
		
		this.destinationX = destinationX;
		this.destinationY = destinationY;
		
		this.parent = parent; //parent is the main canvas wich invoque
		this.canvas = document.createElement('canvas'); //
		this.ctx = this.canvas.getContext('2d'); //get canvas drawing context
		
		this.loadImage(img_file); // load image for tile
		
		if ( typeof Tile.id == 'undefined' ) Tile.id = 0;
		
		//appeler le nom de la classe comme variable statique
		this.id = Tile.id++;
		
		console.log("loading Tile "+this.id);
	}
	
	this.loadImage = function(img_file){  //loads image to draw for tile
		
		this.image = new Image();  //create new image object
		this.image.src = img_file; //load file into image object
		
		var tile = this;
		this.image.onload = function(){  //event handler for image load 
			tile.is_ready = true; // tile is ready when image is loaded
			console.log(tile.image.src+" ready")
		}
		
		return tile;
	}
	
	this.drawImage = function(){ //draw image into tile canvas
		
		this.ctx.clearRect(0,0,this.width,this.height); //clear previous frame
		
		if(this.is_ready){ //do not draw if tile is not ready
			this.ctx.drawImage(this.image, this.x, this.y, this.width, this.height, 0, 0, this.width, this.height);
		} 
	}

};

TileSet = 'Graphics/Tilesets/tileset_exterieur.png';
TileSetBasic = 'Graphics/Tilesets/basic.png';