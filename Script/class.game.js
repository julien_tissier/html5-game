/*
Classe Game
Permet de créer le cadre du jeu et collectionne les objets à afficher
*/

function Game() { //main canvas and demo container
	this.canvas = null; //main canvas object
	this.ctx = null; //main canvas drawing context
	this.mainSprite = 0; //sprite object
	this.mainAvatar = 0;
	
	this.sprites; // sprite objects
	this.tiles; // Tile objects
	this.foregrounds; //Foreground objects
	this.obstacles; //Obstacles objects
	this.battle;
	this.width = 0;
	this.height = 0;
	this.timer = 0;  //hold reference to game loop timer
	this.map; //map image - for now is drawn right onto main canvas which is not ideal
	this.battleground; //battleground image - for now is drawn right onto main canvas which is not ideal
	this.mapX = 0;
	this.mapY = 0;
	this.foreground; //foreground object
	this.transition; //transition object
	this.is_ready;
	this.fontText = "20px pokemon_rs_intlregular";
	
	this.is_fighting = false;
	this.step_fight = -1; //-1: no fight, 0: start fight, 1: end fight
	
	this.is_in_menu = false;
	this.is_pausing = false;
	
	this.audio = new Audio();
	this.mute = false;
	this.music_path;
	this.music_ext;
	
	this.fpsFilter = 40;  //set up improvised game loop at 25 FPS (1000/40 = 25)
	
	this.lastLoop = new Date().getTime();
	
	this.init = function(element){ //initialize game
		
		
		this.is_ready = false;
		
		this.sprites = new Array();
		this.tiles = new Array();
		this.foregrounds = new Array();
		this.obstacles = new Array();
		
		this.canPlayAudio();
		
		this.battleground = new Image();  //create new image object

		this.canvas = document.getElementById(element);  //get canvas element from html
		
		this.width = this.canvas.width;
		this.height = this.canvas.height;
		
		this.ctx = this.canvas.getContext('2d'); //create main drawing canvas
		
		if (!this.ctx) {
			alert("Your browser does not support Canvas");
			return;
		}
		
		var new_canvas = document.createElement('canvas');
		this.transition = new Transition();
		this.transition.init(this, new_canvas);
		
		this.transition.state = 1;

	}
	
	
	this.loadMap = function(img_file){  //loads image to draw for game
		
		this.map = new Image();  //create new image object
		this.map.src = img_file; //load file into image object
		
		var game = this;
		this.map.onload = function(){  //event handler for image load 			
			game.is_ready = true; // game is ready when image is loaded
		}
		
	}
	
	
	this.loadTiledMap = function(src){  //loads json Tiled map to draw for game
		
		var xhr = getXMLHttpRequest(); //
		var fighter = this;
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
				
				var map = jsonParse(xhr.responseText);
				console.log(map);
			}
		};
		
		xhr.open("GET", src, true);
		xhr.send(null);
		
	}
	
	this.loadBattleGround = function(img_file){  //loads image to draw for game
		
		this.battleground = new Image();  //create new image object
		this.battleground.src = img_file; //load file into image object
		
	}
	
	this.loadImage = function(img_file){  //loads image to draw for game
		
		/*var map = new Image();  //create new image object
		map.src = img_file; //load file into image object
		
		var game = this;
		this.map.onload = function(){  //event handler for image load 			
			game.is_ready = true; // game is ready when image is loaded
		}*/
		
	}
		
	this.canPlayAudio = function() {
		
		if(typeof this.audio.canPlayType === "function") {
			
			if(this.audio.canPlayType("audio/ogg") !== "") {				
				this.music_path = "Audio/ogg/";
				this.music_ext = "ogg";
				
			} else if(this.audio.canPlayType("audio/mpeg") !== "") {
				
				this.music_path = "Audio/mp3/";
				this.music_ext = "mp3";
				
			} else if(this.audio.canPlayType("audio/wav") !== "") {
				
				this.music_path = "Audio/wav/";
				this.music_ext = "wav";
				
			} else {
				console.log("Wow, it is time to change your browser !");
			}
		} else {
			console.log("Your browser does not support Audio Element !");
		}
		
		if(this.music_ext != "") {
			console.log("You can play "+this.music_ext+" audio files !");
		}
	}
	
	this.loadAudio = function(uri) {
		
		this.audio = new Audio();
		
		console.log("loading "+this.music_path+uri+"."+this.music_ext);
		
		this.audio.src = this.music_path+uri+"."+this.music_ext;
		
		var game = this;
		
		this.audio.addEventListener('ended', function() { 
		   console.log('ended');
		}, false);

		this.audio.addEventListener('canplaythrough', function() { 
			game.audio.play();
			
			game.audio.muted = !game.mute;
			game.audio.muted = game.mute;
		}, false);
		
		this.audio.addEventListener('volumechange', function() { 
			console.log('volumechange : muted ?', game.audio.muted);
		}, false);
		
		
	}
	
	this.launch = function() {		
		
		var game = this;
		this.timer = setInterval(function(){game.drawFrame(game)}, game.fpsFilter);
	}
	
	this.drawFrame = function(game){ //main drawing function
		
		game.ctx.clearRect(0,0,game.canvas.width, game.canvas.height);  //clear main canvas
		
		if(game.is_ready) {
			
			if (!game.is_fighting) {	
				
				if(game.audio.paused) {
					game.audio.pause();
					
					i =Math.floor((Math.random()*playlist.length));
					//game.loadAudio( playlist[i]);
					
					game.step_fight = -1;
				}
				
				game.drawMap();
				
				if(game.is_in_menu) {
					game.drawMenu();
				}
				
				game.drawHour();
				
			} else {	
				game.drawBattle();
			}
			
			game.drawRain();
				
			// jour/nuit
			var heure=new Date().getHours();
			if (heure<=8 || heure>=18) {
				//ajoute filtre bleu nuit
				game.ctx.fillStyle = "rgba(15, 5, 100, 0.5)";
				game.ctx.fillRect (0, 0, game.width, game.height);
			}
			
			
			if(game.transition.state == 0) {
				game.showTransition();
				
				if(!game.is_fighting && game.step_fight == -1) {
					game.audio.pause();
					//game.loadAudio('Battle/battle KK');
					
					game.step_fight = 0;
				}
				
				if (game.transition.state==1 && game.is_fighting) {

					game.battle = new Battle();
					game.battle.init(game);
					
				}
			} 
		
		} else {
		
			game.ctx.font = game.ctx.font;
			game.ctx.fillStyle = "rgb(0,20,180)";
			game.ctx.fillText("Loading...", game.canvas.width-textWidth("Loading...", game.ctx.font)-20, game.canvas.height-20);
		
		}  	
		
		var thisLoop = new Date().getTime(); 
		//
		var fps = Math.round(1000/(thisLoop - this.lastLoop));
		
		this.lastLoop = thisLoop;

		game.ctx.font = game.fontText;
		game.ctx.fillStyle = "rgb(0,20,180)";
		
		game.ctx.fillText(fps+" fps", 20, game.canvas.height-20);

	}
	
	this.drawHour = function () {
		
		var date = new Date();
		var H= date.getHours();
		var m= date.getMinutes();
		
		var text = H+":"+m;
		
		this.ctx.font = this.fontText;
		this.ctx.fillStyle = "rgb(255,255,255)";
		this.ctx.fillText(text, this.canvas.width-textWidth(text, this.ctx.font)-20, textHeight(text)+20);
		
	}
	
	this.drawRain = function() {
        
		//environnement pluie
		var rainArray = [];
		var maxRainDrops = 10; // Here you may set max flackes to be created 
	
		for (i = 0; i < maxRainDrops; i++) {
			rainArray[i] = ([Math.floor(Math.random() * (this.width+50)), Math.floor(Math.random() * (this.height+50))]);
			
			var srcX = rainArray[i][0]-50;
			var srcY = rainArray[i][1]-50;
			
			var destX = srcX+50;
			var destY = srcY+150;
			this.ctx.beginPath();
			this.ctx.moveTo(srcX, srcY);
			this.ctx.lineTo(destX, destY);
			this.ctx.strokeStyle ='white';
			this.ctx.stroke();
			this.ctx.closePath();
		}      
	
	};
	
	this.drawMap = function() {
		/*
		//dessine un fond
		coef = 32;
		for (i=0, il=this.height/coef;i<=il;i++){
			for (j=0, jl=this.width/coef;j<=jl;j++){
				this.ctx.drawImage(this.map,j*coef,i*coef,coef,coef);
			}
		}*/		
		//
		
		var mapX = this.mapX;
		var mapY = this.mapY;
		
		var srcX = (mapX>=0)?(mapX+this.width>=this.map.width)?this.map.width-this.width:mapX:0;
		var srcY = (mapY>=0)?(mapY+this.height>=this.map.height)?this.map.height-this.height:mapY:0;
		
		var destX = (mapX>=0)?(mapX+this.width>=this.map.width)?srcX-mapX:0:0-mapX;
		var destY = (mapY>=0)?(mapY+this.height>=this.map.height)?srcY-mapY:0:0-mapY;

		// draw map
		this.ctx.drawImage(this.map, srcX, srcY, this.width, this.height, destX, destY, this.canvas.width, this.canvas.height);
		
		for (i=this.tiles.length-1, o=0; i >= o; i--) {
			var tile = this.tiles[i];
			tile.drawImage(); //cause tile to draw itself on its internal canvas
			this.ctx.drawImage(tile.canvas, tile.destinationX-mapX, tile.destinationY-mapY);//draw tile canvas onto main canvas
		}
		
		for (i=this.sprites.length-1, o=0; i >= o; i--) {
			var sprite = this.sprites[i];
			
			sprite.drawImage(); //cause sprite to draw itself on its internal canvas
			
			if (sprite.is_main) {
				this.ctx.drawImage(sprite.canvas, sprite.x, sprite.y);//draw sprite canvas onto main canvas
				sprite.drawlines();
			} else {
				this.ctx.drawImage(sprite.canvas, sprite.x-mapX, sprite.y-mapY);//draw sprite canvas onto main canvas
			}
			
			sprite.drawBubble(); 					
			
		}	
		
		//dessine les avant-plans
		// décalage du aux calculs des sprites précédents
		for (i=this.foregrounds.length-1, o=0; i >= o; i--) {
			var foreground = this.foregrounds[i];
			foreground.ctx.globalAlpha = 0.8;
			foreground.drawImage(); //cause foreground to draw itself on its internal canvas
			
			this.ctx.drawImage(foreground.canvas, foreground.destinationX-mapX, foreground.destinationY-mapY);//draw foreground canvas onto main canvas
		}
		
		// mode debug
		//dessine les limites des obstacles
		for (i=this.obstacles.length-1, o=0; i >= o; i--) {
			var obstacle = this.obstacles[i];
			this.ctx.strokeStyle = "rgb(150,29,28)";
			this.ctx.lineWidth   = 1;
			this.ctx.strokeRect(obstacle.left-mapX,obstacle.top-mapY,obstacle.width,obstacle.height);
		}
	}
	
	this.drawBattle = function() {
	
		this.battle.drawImage();
		this.ctx.drawImage(this.battle.canvas, 0, 0, this.battle.width, this.battle.height); //draw battle canvas onto main canvas

	}
	
	this.drawMenu = function() {
		mainSprite.stopMovement();
		//ajoute un filtre opaque
		this.ctx.fillStyle = "rgba(0, 0, 0, 0.5)";
		this.ctx.fillRect (0, 0, this.width, this.height);
		
		
		
		var mainAvatar = this.mainAvatar;
		var mainAvatarHeight = mainAvatar.height*2;
		var mainAvatarWidth = mainAvatar.width*4;
		
		mainAvatar.drawImage(); //cause foreground to draw itself on its internal canvas
		this.ctx.drawImage(mainAvatar.canvas, 20, (this.height-mainAvatarHeight)/2, mainAvatarWidth, mainAvatarHeight);//draw foreground canvas onto main canvas

		var x = mainAvatarWidth/2;
		var y = 20;
		var h = this.height-40/*(textes.length+1)*10*/;
		var radius = h * 0.1;
		
		var w = this.height-mainAvatarWidth/2/*textWidth("menu")*/+radius*4-40;
		var r = x + w;
		var b = y + h;

		this.ctx.beginPath();
		this.ctx.strokeStyle="black";
		this.ctx.fillStyle="rgba(255, 255, 255, 0.5)";
		this.ctx.lineWidth="1";

		this.ctx.moveTo(x+radius, y);
		this.ctx.lineTo(r-radius, y);
		this.ctx.quadraticCurveTo(r, y, r, y+radius);
		this.ctx.lineTo(r, y+h-radius);
		this.ctx.quadraticCurveTo(r, b, r-radius, b);
		this.ctx.lineTo(x+radius+10, b);
		this.ctx.quadraticCurveTo(x, b, x, b-radius);
		this.ctx.lineTo(x, y+radius);
		this.ctx.quadraticCurveTo(x, y, x+radius, y);
		
		this.ctx.fill();
		this.ctx.stroke();
		
		
		this.ctx.fillStyle="black";
		this.ctx.font = "40px pokemon_rs_intlregular";
		
		this.ctx.fillText("Pause", x+radius, y+radius+10);
	}
	
	this.fight = function() {
		rpg.transition.state = 0;
		rpg.showTransition();
	}
	
	this.addSprite = function(posX, posY, image, ismain) {
				
		is_main = (ismain || false);
		
		var sprite = new Sprite(); //create new Sprite object
		
		if(is_main) {
			this.mainSprite = sprite;
			
			var name = basename(image);			
			this.addAvatar(0, 0, name, ismain);
		}
		
		sprite.init(this, 0, 0, image, is_main); //init sprite
		
		this.sprites.push(sprite);
		
		var game = this;
		
		
		if(!is_main) {
			this.canvas.addEventListener("mousemove",function follow(e) {
				var mx = e.clientX - this.clientLeft;
				var my = e.clientY - this.clientTop;
			  
				var sourisX = event.pageX-$('#main').position().left;
				var sourisY = event.pageY-$('#main').position().top;

				// for each circle...
				//for (i=game.sprites.length-1, o=0; i >= o; i--) {
				//var sprite = game.sprites[i];
				
				if ((sourisX-sprite.x)>=0-game.mapX && (sourisX-sprite.x)<=sprite.width-game.mapX) {
					if ((sourisY-sprite.y+game.mapY)>=0 && (sourisY-sprite.y+game.mapY)<=sprite.height) {
						//console.log("mouseover "+sprite.id);
						sprite.talk("Hello !");
					}
				}
				
				//}	
			},false);

		}
		
			return sprite;
	}	
	
	this.addAvatar = function(posX, posY, image, ismain) {
				
		is_main = (ismain || false);
		
		var avatar = new Avatar(); //create new avatar object
		
		if(is_main) {
			this.mainAvatar = avatar;
		}
		
		avatar.init(this, posX, posY, image, is_main); //init avatar
		
		//this.avatars.push(avatar);

		return avatar;
	}	
	
	this.addTile = function(posX, posY, width, height, destX, destY, image) {
		
		var tile = new Tile(); //create new Tile object
		tile.init(this, posX, posY, width, height, destX, destY, image); //init tile
		
		this.ctx.drawImage(tile.canvas, tile.x, tile.y);//draw sprite canvas onto main canvas
		
		this.tiles.push(tile);
		
		//ajoute le tile en tant qu'obstacle
		//this.addObstacle(destX, destY, width, height, 1); //obstacle	
		
		return tile;
	}	
	
	this.addFighter = function(image, number, orientation) {
		
		var fighter = new Fighter(); //create new Fighter object
		fighter.init(this, image, number, orientation); //init Fighter
		
		this.ctx.drawImage(fighter.canvas, fighter.x, fighter.y);//draw sprite canvas onto main canvas
		
		this.fighters.push(fighter);
		
		return fighter;
	}	
	
	this.addForeground = function(posX, posY, width, height, destX, destY, image) {
		
		var foreground = new Tile(); //create new Tile object
		foreground.init(this, posX, posY, width, height, destX, destY, image); //init foreground
		
		this.ctx.drawImage(foreground.canvas, foreground.x, foreground.y);//draw sprite canvas onto main canvas
		
		this.foregrounds.push(foreground);
		
		return foreground;
	}
	
	this.addObstacle = function(x, y, w, h, nv) {
		
		// on créé un objet vite fait (on verra la suite hein) !
		var obstacle = {
			left: x,
			top: y,
			width: w,
			height: h,
			lvl: nv
		};
		
		this.obstacles.push(obstacle);
		
		return obstacle;
	}
	
	this.showTransition = function(/*posX, posY, width, height, destX, destY, image*/) {
		
		//this.transition.drawCircle();
		this.transition.drawForm();
		
		if(this.transition.state==1) {
			
			this.is_fighting = !this.is_fighting;			
			
			if(!this.is_fighting) {
				this.audio.pause();
			}
			console.log("fighting ?", this.is_fighting);
			
			
		}
	}

	
}

playlist = new Array( 
	'Exploration/0_newbarktown', 
	'Exploration/carmin sur mer', 
	'Exploration/gym', 
	'Exploration/pallettown', 
	'Exploration/pallettown1',
	'Exploration/1G/PkmRB-1', 
	'Exploration/2G/pkgsc_vermillion', 
	'Exploration/3G/3G_Ville 1'
);
