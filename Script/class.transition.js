/*
Classe Sprite
Permet de cr�er un objet anim� (personnage, v�hicule ou monstre)
*/

function Transition() {  
	this.id; //identifiant, utiliser comme variable statique
	this.currentFrame = 0; //current animation frame for this sprite
	this.canvas = null; //canvas to hold this sprite - will be drawn to main canvas
	this.ctx = null; //context for sprite canvas
	this.x = 0; // X position
	this.y = 0; //Y position
	this.width = 0; 
	this.height = 0;
	this.currentStep = 0; //number of frames since this sprite's animation was updated
	this.is_ready = 0; //sprite has finished loading and can be used
	this.timer = 0;
	this.radius = 0;
	this.state = 0;
	
	this.init = function(parent, canvas){ //initialize sprite
		this.is_ready = true; //sprite not ready
		this.x = (parent.canvas.width / 2);
		this.y = (parent.canvas.height / 2);
		this.parent = parent; //parent is the main canvas wich invoque
		this.canvas = canvas; //canvas is created by Game object and passed to sprite
		this.ctx = this.canvas.getContext('2d'); //get canvas drawing context
		this.timer = 0;
		this.state = 0;
		
	}
	
	this.loadImage = function(img_file){  //loads image to draw for sprite
		this.image = new Image();  //create new image object
		this.image.onload = function(){  //event handler for image load 
			this.is_ready = true; // sprite is ready when image is loaded
		}
		this.image.src = img_file; //load file into image object
	}
	
	this.drawImage = function(){ //draw image into sprite canvas
		this.ctx.clearRect(0,0,this.width,this.height); //clear previous frame
		if(this.is_ready){ //do not draw if sprite is not ready
			//calculate values for sprite based on animation
			var srcX = this.animation.sX + (this.currentFrame * this.animation.width);
			var srcY = this.animation.sY ;
			var srcWidth = this.animation.width;
			var srcHeight = this.animation.height;
			
			this.ctx.drawImage(this.image, srcX, srcY, srcWidth, srcHeight, 0, 0, srcWidth, srcHeight); //draw image
			this.stepSprite(); //advance animation
			this.moveSprite(); //move sprite			
	
		}
	}
	
	this.drawCircle = function(){ //draw image into sprite canvas
		
		/*var centerX = this.parent.canvas.width / 2;
		var centerY = this.parent.canvas.height / 2;*/
	 
		this.parent.ctx.beginPath();
		this.parent.ctx.arc(this.x, this.y, this.radius, 0, 3 * Math.PI, false);
		this.parent.ctx.fillStyle = "#FFF";		
		this.parent.ctx.strokeStyle = "#FFF";
		this.parent.ctx.fill();
		this.parent.ctx.stroke();
		
		this.radius=this.radius+10;
		
		if(this.radius>=this.parent.canvas.width/1.5) {
			
			this.state = 1;
			
			return this.state;
		}
		
	}
	
	this.drawForm = function(){ //draw image into sprite canvas
		
		var blockW = this.radius;
		var blockX = this.x-(blockW/2);
		var blockY = this.y-(blockW/2);
		
		var context = this.parent.ctx;
		//rectangle de fond.
		context.fillStyle   = '#fff';
		context.lineWidth   = 1;
		context.fillRect  (blockX, blockY, blockW, this.radius);
		
		//valeurs pour la pokeball
		if (this.radius<=this.parent.canvas.height/2) {
			radius = this.radius;
			miniradius = (this.radius<25)?0:this.radius-25;
		}
		
		// fond noir
		context.beginPath();
		context.arc(this.x, this.y, radius, 0, 3 * Math.PI, false);
		context.fillStyle = "#000";		
		context.fill();		
		context.lineWidth = 10;
		
		//demi-cercle blanc
		context.beginPath();  
		context.arc(this.x, this.y+context.lineWidth, miniradius, 0, Math.PI, false);  
		context.fillStyle = '#fff';
		context.fill(); 
		
		//demi-cercle rouge
		context.beginPath();  
		context.arc(this.x, this.y-context.lineWidth, miniradius, 0, Math.PI, true);  
		context.fillStyle = '#f00';
		context.fill(); 
		
		
		context.beginPath();
		context.arc(this.x, this.y, radius/5, 0, 3 * Math.PI, false);
		context.fillStyle = "#fff";		
		context.strokeStyle = '#000';
		context.lineWidth = 10;
		context.fill();	
		context.stroke();
		
		context.beginPath();
		context.arc(this.x, this.y, radius/8, 0, 3 * Math.PI, false);
		context.fillStyle = "#fff";		
		context.strokeStyle = '#000';
		context.lineWidth = 3;
		context.fill();	
		context.stroke();
				
		if(this.radius>=this.parent.canvas.width) {
			this.state = 1;
			this.radius = 0;
		}
		
		// incr�mentation
		if (this.radius<=this.parent.canvas.height/2) {
			this.radius=this.radius+10;
		} else {
			// si la forme est termin�e
			this.radius=this.radius+50;
		}
	}
	
	

} 