/**/
jQuery(document).ready(function($) {
	
	$('#chatbox form').submit(function() {
		mainSprite.talk($('#chatinput').val());
		
		$('#chatinput').val(''); //on vide le champ
		$('#main').focus(); //on rend le focus sur le canvas
		return false;
	});
	
		
	$('#chatinput').blur(function() {
		$('#chatinput').animate({
			'margin-top': '0'
		}, function() {
			$(this).removeClass('active');
		});
		moveLock=false;
	}).focus(function() {
		$('#chatinput').animate({
			'margin-top': '-42px'
		}, function() {
			$(this).addClass('active');
		});

		moveLock=true;
	});


	
	/*************************** User Interface ***************************/

			
	var t;
	$('#addSprite').click(function () {
		val = $(this).val();
		
		if (t == undefined) {
			t = setInterval(function() {
				i =Math.floor((Math.random()*characters.length));
				unSprite = rpg.addSprite(rpg.width/2+rpg.mapX, rpg.height/2+rpg.mapY, characters[i]);
				randomMove(unSprite, rpg);
				$('#listSprite').prepend(unSprite.id+' : '+basename(characters[i], '.png')+'<br />')
				
			},100);
		
			$(this).val("arr�ter la g�n�ration");

			
		} else {
			clearInterval(t);
			t = undefined;
			
			$(this).val("G�n�rer des personnages");
		}
		
	});
	
	$('#Sparta').click(function () {
		indice=1
		while (indice<=300){
			
			i =Math.floor((Math.random()*characters.length))
			unSprite = rpg.addSprite(rpg.width/2+rpg.mapX, rpg.height/2+rpg.mapY, characters[i]);
			
			$('#listSprite').prepend(unSprite.id+' : '+basename(characters[i], '.png')	+'<br />')
			
			randomMove(unSprite, rpg);
			indice++;
		}
		
		
	});	
	
	$('#fight').click(function () {	
		rpg.fight();
	});
	
	for (i=0, il=characters.length;i<il;i++){
		$('#charOption').append('<li id="'+i+'">'+basename(characters[i], '.png')+'</li>');
	}
	

	$('#charOption li').each(function(index) {
		$(this).css('background-image','url("'+characters[index]+'")');	
		if(index==mainIndex)
			$(this).attr('class', 'default');
			
	});
	
	defaultIndex = mainIndex;
	
	$('#charOption li').click(function() {
		index = $(this).attr('id');
		if(index==defaultIndex) {
			$(this).parent().css({
							'height': '300px',
							'overflow-y': 'visible'
						});
			$('#charOption li').css('display','block');
		} else {
			$(this).parent().css({
								'height': $(this).css('height'),
								'overflow-y': 'hidden'
							});
			$('#charOption li').css('display','none');
			$(this).css('display','block');
			defaultIndex = index;
			mainSprite.image.src = characters[defaultIndex];
			rpg.mainAvatar.image.src = avatars[defaultIndex];
		}
	});
	
	 $('#stats, #fps, #ms').on({
        // on commence le drag
        dragstart: function(e) {
            $this = $(this);
            i = $(this).index();
            $(this).css('opacity', '0.5');
			console.log("drag element");
        },
        // declenche tant qu on a pas lache l element
        dragover: function(e) {
            e.preventDefault();
        },
        // le drop est termine, fin du drag
        dragend: function() {
            $(this).css('opacity', '1');
        }
    });
			
});