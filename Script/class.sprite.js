/*
Classe Sprite
Permet de cr�er un objet anim� (personnage, v�hicule ou monstre)
*/

function Sprite() {  
	this.id; 				// id
	this.parent;			// main canvas
	this.canvas;			// canvas to hold this sprite - will be drawn to main canvas
	this.ctx;				// context for sprite canvas
	this.x = 0;				// X position of this sprite
	this.y = 0;				// Y position of this sprite	
	this.destX = -1;		// X position of this sprite
	this.destY = -1;		// Y position of this sprite	
	this.FrameX = 4;		// 
	this.FrameY = 4;		
	this.animation = 0;		// current animation for this sprite
	this.currentFrame = 0;	// current animation frame for this sprite
	this.width = 0;			
	this.height = 0;		
	this.image = 0;			// image that is being drawn to the canvas
	this.currentStep = 0;	// number of frames since this sprite's animation was updated
	this.moveStep = 8;
	this.is_ready;			// sprite has finished loading and can be used
	this.is_main;
	this.timer = 0;
	this.message = '';
	this.messageTimer = 0;
	this.currentCollision = false;
      	
	this.init = function(parent, x, y, img_file, is_main){ //initialize sprite
		this.is_ready = false; //sprite not ready
		this.is_main = is_main; //sprite not ready
		
		if(!this.is_main) {
			this.x = x;
			this.y = y;
		} else {
			this.x = parent.width/2;
			this.y = parent.height/2;
		}
		this.parent = parent; //parent is the main canvas wich invoque
		this.canvas = document.createElement('canvas'); //
		this.ctx = this.canvas.getContext('2d'); //get canvas drawing context
		
		this.loadImage(img_file); // load image for sprite
		
		this.animation = standBottom; //set initial animation set
		this.timer = 0;
		
		if ( typeof Sprite.id == 'undefined' ) Sprite.id = 0;
		
		//appeler le nom de la classe comme variable statique
		this.id = Sprite.id++;
		
	}
	
	this.loadImage = function(img_file){  //loads image to draw for sprite
		
		this.image = new Image();  //create new image object
		this.image.src = img_file; //load file into image object
		
		var sprite = this;
		this.image.onload = function(){  //event handler for image load 
			sprite.width = this.width/sprite.FrameX;
			sprite.height = this.height/sprite.FrameY;
			
			sprite.is_ready = true; // sprite is ready when image is loaded
		}
		
		return sprite;
	}
	
	//function called in game
	this.drawImage = function(){ //draw image into sprite canvas
		this.ctx.clearRect(0,0,this.width,this.height); //clear previous frame
		
		if(this.is_ready){ //do not draw if sprite is not ready
			//calculate values for sprite based on animation
			var srcX = this.animation.sX + (this.currentFrame * this.animation.width);
			var srcY = this.animation.sY ;
			
			var srcWidth = this.animation.width;
			var srcHeight = this.animation.height;
			
			
			if(this.currentCollision == "grass") {
				srcHeight = this.animation.height/1.5;
			}
			
			this.ctx.drawImage(this.image, srcX, srcY, srcWidth, srcHeight, 0, 0, srcWidth, srcHeight); //draw image
			this.stepSprite(); //advance animation
			this.moveSprite(); //move sprite			
	
		}
	}

	this.drawlines = function() {
	
		var destX = (this.is_main)?this.destX-this.parent.mapX:this.destX;
		var destY = (this.is_main)?this.destY-this.parent.mapY:this.destY;
	
		if(destX!=-1 && destY!=-1 && this.timer != 0) {
		
			posx = (this.x+8>destX)?destX+this.width/2:destX-this.width/2;
			posy = (this.y+8>destY)?destY+this.height/2:destY-this.height/2;
			
			this.ctx.lineWidth = 1;
			this.parent.ctx.beginPath();
			this.parent.ctx.strokeStyle="rgb(255, 255, 255)";
			this.parent.ctx.moveTo(this.x+this.width/2, this.y+this.height/2);
			this.parent.ctx.lineTo(posx, this.y+this.height/2);
			this.parent.ctx.lineTo(posx, posy);
			this.parent.ctx.stroke();
		}
	}
	
	this.drawBubble = function() {
		
		var timestamp = new Date().getTime();
		if(this.message != '' && timestamp-this.messageTimer<3000) {
		
			var words = this.message.split(" ");
			var textes = new Array();
			var i = 0;
			var j = 0;
			var texte = '';
			
			while (i<=words.length) {
				//alert(i+" modulo 5="+i%5);
				if (i==0 || i<words.length && i%5!=0) {
					texte = texte+" "+words[i]+"";
				} else {
					textes[textes.length] = texte;
					//alert(texte);
					texte = words[i];
				}
				i++;
			}
			
			var x = this.x-this.parent.mapX+this.width/2;
			var y = this.y-this.parent.mapY-this.height;
			
			if (this.is_main) {
				var x = this.x+this.width/2;
				var y = this.y-this.height/2;
			} 
			
			var h = (textes.length+1)*10;
			var radius = h * 0.1;
			
			var w = textWidth(textes)+radius*4;
			var r = x + w;
			var b = y + h;
			
			this.parent.ctx.beginPath();
			this.parent.ctx.strokeStyle="black";
			this.parent.ctx.fillStyle="rgba(255, 255, 255, 0.5)";
			this.parent.ctx.lineWidth="1";

			this.parent.ctx.moveTo(x+radius, y);
			this.parent.ctx.lineTo(r-radius, y);
			this.parent.ctx.quadraticCurveTo(r, y, r, y+radius);
			this.parent.ctx.lineTo(r, y+h-radius);
			this.parent.ctx.quadraticCurveTo(r, b, r-radius, b);
			this.parent.ctx.lineTo(x+radius+10, b);
			this.parent.ctx.lineTo(x+radius/2, b+10);
			this.parent.ctx.lineTo(x+radius, b);
			this.parent.ctx.quadraticCurveTo(x, b, x, b-radius);
			this.parent.ctx.lineTo(x, y+radius);
			this.parent.ctx.quadraticCurveTo(x, y, x+radius, y);
			
			this.parent.ctx.fill();
			this.parent.ctx.stroke();
			
			
			this.parent.ctx.fillStyle="black";
			this.parent.ctx.font = "10px pokemon_rs_intlregular";
			
			this.parent.ctx.fillText(textes, x+radius, y+radius+10);
			/*i=0;
			while (i<textes.length) {
				this.parent.ctx.fillText(textes[i], x+radius, y+radius+i*10+10);
				i++;
			}*/
			
		}
		
	}
	
	this.stepSprite = function(){ //advance animation based on animation speed (step value)
		if(this.currentStep >= this.animation.step){
			this.currentStep = 0;
			this.currentFrame++;
			if(this.currentFrame >= this.animation.totalFrames){ 
				if(this.animation.loop){
					this.currentFrame = 0; //loop animation back to start
				} else {
					this.currentFrame = this.animation.totalFrames -1;	//if loop not set, hold on final frame
				}
			}
		}
		else {
			this.currentStep++; //advance step counter if step limit not reached	
		}
	}
	
	
	this.moveSprite = function(){
		
		collision = this.checkCollision();
		
		if(!collision) {
			if(this.animation.name == walkRight.name){
				if(this.is_main) {
					
					if(this.is_main) {
						this.parent.mapX += this.moveStep;
					} else {
						this.x += this.moveStep;
					}
					
					/*
					// if we are in the map
					if((this.x + this.parent.mapX + this.width) < this.parent.map.width) {
						// if we have to scroll the map
						if(this.x > this.parent.canvas.width - this.width) {
							//this.x = 0 - this.width;
							this.parent.mapX += this.moveStep;
						} else {
							this.x += this.moveStep;
						}
					}
					*/
				} else {
					this.x += this.moveStep;
				}
			}
			else if(this.animation.name == walkLeft.name){
				
				if(this.is_main) {
					this.parent.mapX -= this.moveStep;
				} else {
					this.x -= this.moveStep;
				}
				
			}
			else if(this.animation.name == walkTop.name){
						
				if(this.is_main) {
					this.parent.mapY -= this.moveStep;
				} else {
					this.y -= this.moveStep;
				}
				
			}
			else if(this.animation.name == walkBottom.name){
				
				if(this.is_main) {
					this.parent.mapY += this.moveStep;
				} else {
					this.y += this.moveStep;
				}
			
			}
			
			if(this.currentFrame>0 && this.currentCollision =="grass" && !this.is_fighting && this.is_main) {
				this.parent.fight();
			}
			
		} else {
			this.stopMovement();
		}
		
	}
	
	this.moveTo = function(posx, posy) {
	
		// calcul du plus court chemin => voir algotithme path finding
		if(this.is_main) {
			this.destX = (this.x<posx)?posx+this.parent.mapX+this.width/2:posx+this.parent.mapX-this.width/2;
			this.destY = (this.y<posy)?posy+this.parent.mapY+this.height/2:posy+this.parent.mapY-this.height/2;
		} else {
			this.destX = (this.x<posx)?posx+this.width/2:posx-this.width/2;
			this.destY = (this.y<posy)?posy+this.height/2:posy-this.height/2;
		}
		var sprite = this;

		clearInterval(this.timer);
		this.timer = setInterval(function(){sprite.moveFrame()}, 40);
		
	}
	
	this.moveFrame = function() {
		
		var collision = this.checkCollision();
		if(!collision) {
			if(this.is_main) {
				
				var posX = this.x+this.parent.mapX;
				var posY = this.y+this.parent.mapY;
				
				if (posX <= this.destX && posX >= (this.destX-this.width)) {
					if (posY <= (this.destY) && posY >= (this.destY-this.height)) {
						this.stopMovement();
					} else if ((this.destY-posY)<0) {
						this.move('top');
					} else {
						this.move('bottom');
					}
				} else if((this.destX-posX)<0) {
					this.move('left');
				} else if((this.destX-posX)>0){
					this.move('right');
				}
			
			} else {
				if (this.x <= this.destX && this.x >= (this.destX-this.width)) {
					if (this.y <= (this.destY) && this.y >= (this.destY-this.height)) {
						this.stopMovement();
					} else if ((this.destY-this.y)<0) {
						this.move('top');
					} else {
						this.move('bottom');
					}
				} else if((this.destX-this.x)<0) {
					this.move('left');
				} else if((this.destX-this.x)>0){
					this.move('right');
				}
			}
		}
		
	}

	this.randomMove = function(limitX, limitY) {
			
		var x = Math.floor(Math.random()*(this.parent.canvas.width))+limitX;
		var y = Math.floor(Math.random()*(this.parent.canvas.height))+limitXY;
		
		console.log("move to "+x+";"+y);
		this.moveTo(x, y);
		
		setTimeout(function(){this.randomMove(limitX, limitY)}, 3000);

	}

	this.talk = function(text) {
		this.message = text;
		this.messageTimer = new Date().getTime();
	}
	
	this.move = function(direction) {
		
		switch(direction) {
			case 'left':
				this.animation = walkLeft;
				break;
			case 'right':
				this.animation = walkRight;
				break;
			case 'top':
				this.animation = walkTop;
				break;
			case 'bottom':
				this.animation = walkBottom;
				break;
		}

	}
	
	this.stopMovement = function(){
		
		switch(this.animation.name) {
			case walkLeft.name:
				this.animation = standLeft;
				break;
			case walkRight.name:
				this.animation = standRight;
				break;
			case walkTop.name:
				this.animation = standTop;
				break;
			case walkBottom.name:
				this.animation = standBottom;
				break;
		}
		
		this.currentFrame = 0;
		this.currentStep = 0;
		clearInterval(this.timer);
		this.timer = 0;

	}
	
	this.checkCollision = function() {
		var collision = false;
		
		var l = 0; //left
		var t = 0; //top
		var camX = 0;
		var camY = 0;
		
		switch(this.animation.name) {
			case walkLeft.name:
				l=-8;
				break;
			case walkRight.name:
				l=8;
				break;
			case walkTop.name:
				t=-8;
				break;
			case walkBottom.name:
				t=8;
				break;
		}
		
		var left = (this.x+this.width/2)+l;
		var top  = (this.y+this.height/2)+t;
		
		var parent = this.parent;
		var currentCollision = false;
		
		if (this.is_main) {
			camX = parent.mapX;
			camY = parent.mapY;
		}
		
		//if out of map
		if (camX+left < 0 || camY+top < 0 || camX+left > parent.map.width || camY+top > parent.map.height) {
			collision = true;
			console.log(this.id + " : End of map !");		
		} else {
			if(parent.obstacles != undefined) {
				j=0;
				while(j<parent.obstacles.length && !collision) {
				
					obstacle = parent.obstacles[j];
					
					if((left>=(obstacle.left-camX)) && (left<=(obstacle.left+obstacle.width-camX))) {
						if((top>=(obstacle.top-camY)) && (top<=(obstacle.top+obstacle.height-camY))) {
						
							currentCollision = obstacle.lvl;
							if(obstacle.lvl == 1) {
								collision = true;
								console.log(this.id + " : collision !");
							} 
						}
					}		
					
					j++;
				}
			}
		}
		
		if (this.currentCollision != currentCollision) {
			this.currentCollision = currentCollision;
			console.log(this.id + " : " + this.currentCollision);
		}
		
		return collision;
	
	}
	
}  

/****** 
possibilit� d'h�ritage : 
http://www.severnsolutions.co.uk/twblog/archive/2004/10/08/supersimulationjsoop
*****/

function SpriteAnimation(name, totalFrames, loop, step, width, height, sY, sX) {  
	this.name = name; //identifiant, utiliser comme variable statique
	this.totalFrames = totalFrames; //main canvas
	this.loop = loop; //canvas to hold this sprite - will be drawn to main canvas
	this.step = step; //context for sprite canvas
	this.width = width; 
	this.height = height;
	this.sY = sY; //image that is being drawn to the canvas
	this.sX = sX; //number of frames since this sprite's animation was updated
}

/****** Objects to represent animations ******/

var standBottom = new SpriteAnimation('standBottom', 1, true, 4, 64, 64, 0, 0);
var standLeft	= new SpriteAnimation('standLeft', 1, true, 4, 64, 64, 64, 0);
var standRight	= new SpriteAnimation('standRight', 1, true, 4, 64, 64, 128, 0);
var standTop	= new SpriteAnimation('standTop', 1, true, 4, 64, 64, 192, 0);

var walkBottom	= new SpriteAnimation('walkBottom', 4, true, 4, 64, 64, 0, 0);
var walkLeft	= new SpriteAnimation('walkLeft', 4, true, 4, 64, 64, 64, 0);
var walkRight	= new SpriteAnimation('walkRight', 4, true, 4, 64, 64, 128, 0);
var walkTop		= new SpriteAnimation('walkTop', 4, true, 4, 64, 64, 192, 0);

var standBottom_grass	= new SpriteAnimation('standBottom_grass', 1, true, 4, 64, 40, 0, 0);
var standLeft_grass		= new SpriteAnimation('standLeft_grass', 1, true, 4, 64, 40, 64, 0);
var standRight_grass	= new SpriteAnimation('standRight_grass', 1, true, 4, 64, 40, 128, 0);
var standTop_grass		= new SpriteAnimation('standTop_grass', 1, true, 4, 64, 40, 192, 0);

var walkBottom_grass	= new SpriteAnimation('walkBottom_grass', 4, true, 4, 64, 40, 0, 0);
var walkLeft_grass		= new SpriteAnimation('walkLeft_grass', 4, true, 4, 64, 40, 64, 0);
var walkRight_grass		= new SpriteAnimation('walkRight_grass', 4, true, 4, 64, 40, 128, 0);
var walkTop_grass		= new SpriteAnimation('walkTop_grass', 4, true, 4, 64, 40, 192, 0);

/**/
characterPath = 'Graphics/Sprites/characters';
characterAvatarPath = 'Graphics/Avatars/characters/160x160';
monsterPath = 'Graphics/Sprites/monsters';
monsterAvatarPath = 'Graphics/Avatars/monsters/Front_Male';
characters = new Array( 
				characterPath+'/badboy.png', 
				characterPath+'/big.png', 
				characterPath+'/blackhair.png', 
				characterPath+'/blond.png', 
				characterPath+'/boy.png', //personnage principal
				characterPath+'/boyfriend.png', 
				characterPath+'/cook.png', 
				characterPath+'/geek.png', 
				characterPath+'/girl.png', 
				characterPath+'/girlfriend.png', 
				characterPath+'/hairclip.png', 
				characterPath+'/littleboy.png', 
				characterPath+'/mom.png', 
				characterPath+'/mountain.png', 
				characterPath+'/nurse.png', 
				characterPath+'/oldman.png',
				characterPath+'/professor.png',
				characterPath+'/ranger.png',
				characterPath+'/rival.png',
				characterPath+'/scientist.png',
				characterPath+'/scout.png', 
				characterPath+'/worker.png',
				monsterPath+'/006.png'
			);
avatars = new Array( 
				characterAvatarPath+'/badboy.png', 
				characterAvatarPath+'/big.png', 
				characterAvatarPath+'/blackhair.png', 
				characterAvatarPath+'/blond.png', 
				characterAvatarPath+'/boy.png', //personnage principal
				characterAvatarPath+'/boyfriend.png', 
				characterAvatarPath+'/cook.png', 
				characterAvatarPath+'/geek.png', 
				characterAvatarPath+'/girl.png', 
				characterAvatarPath+'/girlfriend.png', 
				characterAvatarPath+'/hairclip.png', 
				characterAvatarPath+'/littleboy.png', 
				characterAvatarPath+'/mom.png', 
				characterAvatarPath+'/mountain.png', 
				characterAvatarPath+'/nurse.png', 
				characterAvatarPath+'/oldman.png',
				characterAvatarPath+'/professor.png',
				characterAvatarPath+'/ranger.png',
				characterAvatarPath+'/rival.png',
				characterAvatarPath+'/scientist.png',
				characterAvatarPath+'/scout.png', 
				characterAvatarPath+'/worker.png',
				monsterAvatarPath+'/006.png'
			);
