Orientation = { 
    back: "Back", 
	front: "Front"
};

/*
Classe Battle
Permet de g�rer les combats
*/
 
function Battle() {  
	
	this.id; 				// id
		
	this.canvas = null; //main canvas object
	this.ctx = null; //main canvas drawing context
	
	this.fighters; // fighter objects
	
	this.width = 0;
	this.height = 0;
	this.timer = 0;  //hold reference to game loop timer
	
	this.background; //battleground image - for now is drawn right onto main canvas which is not ideal
	
	this.battlegrounds; //battleground image - for now is drawn right onto main canvas which is not ideal
	
	this.is_ready;
	
	this.fighters = new Array();
	
	this.is_fighting = false;
	this.step_fight = -1; //-1: no fight, 0: start fight, 1: end fight 
	
	this.audio = new Audio();
	
	this.introduction = [];
	this.introductionIndex = 0;
	
	this.menu;
	
	this.init = function(parent){ //initialize Fighter

		this.battlegrounds = new Array();
		
		this.is_ready = false; //Fighter not ready
		
		this.parent = parent; //parent is the main canvas wich invoque
		this.canvas = document.createElement('canvas'); //
		this.ctx = this.canvas.getContext('2d'); //get canvas drawing context
				
		this.width = this.parent.canvas.width;
		this.height = this.parent.canvas.height;
		
		this.canvas.width = this.parent.canvas.width;
		this.canvas.height = this.parent.canvas.height;
		
		console.log(this.width, this.height);
		
		//character's fighter
		this.addFighter(6, Orientation.back);
		
		//enemy fighter
		i =Math.floor((Math.random()*493));
		this.addFighter(i, Orientation.front);
		
		this.introduction[this.introduction.length] = "Un "+this.fighters[1].name+" sauvage apparait !";
		this.introduction[this.introduction.length] = "Go "+this.fighters[0].name+" !";
		
		//background
		i =Math.floor((Math.random()*7));	
		
		this.loadBackground("Graphics/Battlegrounds/Backgrounds/battle"+i+".png");
		
		this.addBattleground(i, Orientation.back);
		this.addBattleground(i, Orientation.front);
		
		this.menu = new Menu();
		
	}
	
	this.addFighter = function(image, number, orientation) {
		
		var fighter = new Fighter(); //create new Fighter object
		fighter.init(this, image, number, orientation); //init Fighter
		
		this.fighters.push(fighter);
		
	}	
	
	
	this.addBattleground = function(image, orientation) {
		
		var battleground = new Battleground(); //create new Fighter object
		battleground.init(this, image, orientation); //init Fighter
		
		this.battlegrounds.push(battleground);
		
	}	
	
	this.loadBackground = function(img_file){  //loads image to draw for Fighter
		
		console.log("loading "+img_file+"...");
		this.background = new Image();  //create new image object
		this.background.src = img_file; //load file into image object
		
		
		var battle = this;
		this.background.onload = function(){  //event handler for image load 
			battle.is_ready = true; // Fighter is ready when image is loaded
		}
		
		this.background.onerror = function(){  //event handler for image error 
			console.log("Error loading as image "+this.src);
		}
		
	}
	
	this.drawImage = function(){ //draw image into Fighter canvas
		
		this.ctx.clearRect(0,0,this.width,this.height); //clear previous frame
		
		if(this.is_ready){ //do not draw if background is not ready
		
			this.ctx.drawImage(this.background, 0, 0, this.width, this.height/1.5); // draw canvas background
						
			for (i=this.battlegrounds.length-1, o=0; i >= o; i--) {
				var battleground = this.battlegrounds[i];
				battleground.drawImage(); //cause battleground to draw itself on its internal canvas
				this.ctx.drawImage(battleground.canvas, battleground.destinationX, battleground.destinationY);//draw battleground canvas onto battle canvas
			}
			
			for (i=this.fighters.length-1, o=0; i >= o; i--) {
				var fighter = this.fighters[i];
				fighter.drawImage(); //cause Fighter to draw itself on its internal canvas
				this.ctx.drawImage(fighter.canvas, fighter.destinationX, fighter.destinationY);//draw fighter canvas onto battle canvas
			}
			
			this.drawMenu();
			
			this.drawFrontStats();
			this.drawBackStats();
		} else {
			this.ctx.font = "20px pokemon_rs_intlregular";
			this.ctx.fillStyle = "rgb(0,20,180)";
			this.ctx.fillText("Loading...", this.canvas.width-100, this.canvas.height-20);
		
		}
		
	}
	
	this.drawMenu = function() {
		this.ctx.strokeStyle = "rgba(150,29,28, 1)";
			this.ctx.fillStyle = "rgba(255,255,255, 1)";
			this.ctx.lineWidth = 8;
			
			menuX = 0;
			menuY = this.height/1.5;
			menuWidth = this.width;
			menuHeight = this.height-menuY;
			menuFontSize = 40;
			//context.fillRect(x,y,width,height);
			this.ctx.fillRect(menuX,menuY,menuWidth,menuHeight);
			this.ctx.strokeRect(menuX,menuY,menuWidth,menuHeight);
			
			
			this.ctx.fillStyle="black";
			this.ctx.font = menuFontSize+"px pokemon_rs_intlregular";
			var fighter = this.fighters[this.fighters.length-1];
			
			
			this.ctx.fillText(this.introduction[this.introductionIndex], menuX+this.ctx.lineWidth, menuY+menuFontSize);
	}
	
	this.drawFrontStats = function() {
		
		this.ctx.strokeStyle = "rgba(0,0,0, 0.5)";
		this.ctx.fillStyle = "rgba(255,255,255, 0.5)";
		this.ctx.lineWidth = 8;
		
		statX = 20;
		statY = 20;
		statWidth = 300;
		statHeight = 150;
		statFontSize = 20;
		//context.fillRect(x,y,width,height);
		this.ctx.fillRect(statX,statY,statWidth,statHeight);
		this.ctx.strokeRect(statX,statY,statWidth,statHeight);
		
		this.ctx.fillStyle="black";
		this.ctx.font = statFontSize+"px pokemon_rs_intlregular";
		
		this.ctx.fillText("lvl 15", statX+this.ctx.lineWidth, statY+menuFontSize);
		
		
	}
	
	this.drawBackStats = function() {
		
		this.ctx.strokeStyle = "rgba(0,0,0, 0.5)";
		this.ctx.fillStyle = "rgba(255,255,255, 0.5)";
		this.ctx.lineWidth = 8;
		
		statX = this.width-300-20;
		statY = this.height/1.5-150-20;
		statWidth = 300;
		statHeight = 150;
		statFontSize = 20;
		//context.fillRect(x,y,width,height);
		this.ctx.fillRect(statX,statY,statWidth,statHeight);
		this.ctx.strokeRect(statX,statY,statWidth,statHeight);
		
		this.ctx.fillStyle="black";
		this.ctx.font = statFontSize+"px pokemon_rs_intlregular";
		
		this.ctx.fillText("lvl 60", statX+this.ctx.lineWidth, statY+menuFontSize);
		
		
	}

}