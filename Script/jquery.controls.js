/**/
jQuery(document).ready(function($) {
	
	//variable qui permet de bloquer les touches de mouvement
	moveLock=false;
	nbKeyUp = 0;
	lastKeyCode = 0;
	keyUp = [];
	
	$('canvas').click(function(event){
		
		
		if (rpg.is_fighting) {
			console.log("click");
			console.log(rpg.battle.introductionIndex++);
		} else if (!rpg.is_fighting && !rpg.is_in_menu) {
			var sourisX = event.pageX-$('#main').position().left;
			var sourisY = event.pageY-$('#main').position().top;
			console.log(mainSprite.id+" bouge "+sourisX+" "+(sourisY));
			
			mainSprite.moveTo(sourisX, sourisY);
		}
		
	});
	
	$(document).keydown(function(event){
		var code = event.keyCode || event.which;
		$("#key"+code).addClass("active");
		
		if (!rpg.is_fighting) {
			
			if (code === 27){
				rpg.is_in_menu = !rpg.is_in_menu;
				console.log("in menu ? "+rpg.is_in_menu);
			}
		} else {
			console.log("keyDown");
			console.log(rpg.battle.introductionIndex++);
		}

		if (!rpg.is_fighting && !rpg.is_in_menu) {
				
			if (code === 17){
				if (!moveLock){
				  moveLock=true;
				  $("#message").focus();
				}
				else{
				  moveLock=false;
				  $("#message").blur();                
				}
			} else  if(!moveLock){
				switch (code) {   
					case 90:  /* Up arrow was pressed */
						event.preventDefault();
						mainSprite.move('top');
						keyUp[code] = true;
					  break;
					case 83:  /* Down arrow was pressed */
						event.preventDefault();
						mainSprite.move('bottom');
						keyUp[code] = true;
					  break;
					case 81:  /* Left arrow was pressed */
						event.preventDefault();
						mainSprite.move('left');
						keyUp[code] = true;
					  break;
					case 68:  /* Right arrow was pressed */
						event.preventDefault();
						mainSprite.move('right');
						keyUp[code] = true;
					  break;
				}
			}
			
		} 

		
	}).keyup(function(event){
		var code = event.keyCode || event.which;
		$("#key"+code).removeClass("active");
		
		if (!rpg.is_fighting) {
			if (!moveLock){
				switch (code) {  
					
					case 13:  /* Enter was pressed */
						$("#chatinput").focus();
					  break;
					  
					case 90:  /* Up arrow was pressed */
					case 83:  /* Down arrow was pressed */
					case 81:  /* Left arrow was pressed */
					case 68:  /* Right arrow was pressed */
						
						keyUp[code] = false;
						event.preventDefault();
						if(!keyUp[90] && !keyUp[83] && !keyUp[81] && !keyUp[68]) {
							mainSprite.stopMovement();
						} else {
							
							var e = jQuery.Event("keydown");
							var keycode;
							
							if (keyUp[90]) {
								keycode = 90;
							} else if (keyUp[83]) {
								keycode = 83;
							} else if (keyUp[81]) {
								keycode = 81;
							} else if (keyUp[68]) {
								keycode = 68;
							} 
							
							e.which = keycode; // # Some key code value
							e.keyCode = keycode;
								
							$("canvas").trigger(e);
						}
					  break;
				}
			}
		}
	});			
});