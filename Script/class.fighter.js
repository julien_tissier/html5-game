/*
Classe Fighter
Permet de cr�er les combattants
*/
 
function Fighter() {  
	
	this.id; 				// id
	this.number;
	this.name = "monstre";
	this.parent;			//main canvas
	this.canvas;			//canvas to hold this Fighter - will be drawn to main canvas
	this.ctx;				//context for Fighter canvas
	this.x = 0;				// X position of this Fighter
	this.y = 0;				//Y position of this Fighter
	this.destinationX;		
	this.destinationY;		
	this.width = 0; 		
	this.height = 0;		
	this.image;				//image that is being drawn to the canvas
	this.is_ready = 0;		//Fighter has finished loading and can be used
	this.orientation;
	
	this.init = function(parent, number, direction){ //initialize Fighter
		
		this.number = number.toString(); //cast number in string
		
		while (this.number.length <3) {
			this.number = "0"+this.number;
		}
		
		this.getName();
		
		this.is_ready = false; //Fighter not ready
		
		this.orientation = direction;
		
		this.parent = parent; //parent is the main canvas wich invoque
		this.canvas = document.createElement('canvas'); //
		this.ctx = this.canvas.getContext('2d'); //get canvas drawing context
		
		this.loadImage("Graphics/Avatars/monsters/"+this.orientation+"_Male/"+this.number+".png"); // load image for Fighter
		
		if ( typeof Fighter.id == 'undefined' ) Fighter.id = 0;
		
		//appeler le nom de la classe comme variable statique
		this.id = Fighter.id++;
		
		console.log("loading "+this.orientation+" Fighter "+this.id);
	}
	
	this.loadImage = function(img_file){  //loads image to draw for Fighter
		
		this.image = new Image();  //create new image object
		this.image.src = img_file; //load file into image object
		
		
		var fighter = this;
		this.image.onload = function(){  //event handler for image load 
			fighter.width = fighter.image.width;
			fighter.height = fighter.image.height;
			
			if(fighter.orientation === "Back") {
				fighter.destinationX = 0;
				fighter.destinationY = fighter.parent.height/1.25-fighter.image.height;
			} else {
				fighter.destinationX = fighter.parent.width-(fighter.image.width*1.5);
				fighter.destinationY = 20;
			}
		
		
			fighter.is_ready = true; // Fighter is ready when image is loaded
		}
		
		return fighter;
	}
	
	this.getName = function() {
		var xhr = getXMLHttpRequest(); // Voyez la fonction getXMLHttpRequest() d�finie dans la partie pr�c�dente
		var fighter = this;
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
				//alert(xhr.responseText); // Donn�es textuelles r�cup�r�es
				//console.log(xhr.responseText);
				var p = jsonParse(xhr.responseText);
				
				fighter.name = p.name;
			}
		};
		
		var async = false;
		xhr.open("GET", "ajax/name.php?n="+this.number, async);
		xhr.send(null);
		
	}
	
	this.drawImage = function(){ //draw image into Fighter canvas
		
		this.ctx.clearRect(0,0,this.width,this.height); //clear previous frame
		
		if(this.is_ready){ //do not draw if Fighter is not ready
			this.ctx.drawImage(this.image, this.x, this.y, this.width, this.height);
		} 
	}

}